---
title: Wat is het FDS?
weight: 20
date: 2024-04-26
author: R-FDS Team
description: Beschrijving van wat FDS, het Federatief Datastelsel, inhoudt en betekent
---
## Context

Het Federatief Datastelsel komt voort uit het volgende beleidsdoel: “Als overheid benutten we het
volle potentieel van data bij maatschappelijke opgaven, zowel binnen als tussen domeinen, op een
ambitieuze manier die vertrouwen wekt.” (Bron [Meerjarenaanpak Interbestuurlijke
Datastrategie](https://realisatieibds.pleio.nl/files/view/1c9729a6-70a9-4bda-a245-49c1d6d63adb/meerjarenaanpak-interbestuurlijke-datastrategie6maart2023.pdf)).
Voor het realiseren van dit doel zijn in de Meerjarenaanpak Interbestuurlijke Datastrategie op
globaal niveau een aantal functies onderkend (daar “systeemfuncties” genoemd). Eén van deze
systeemfuncties is het Federatief Datastelsel (afgekort “FDS”).

## Algemene scope Federatief Datastelsel

Bij de realisatie van het Federatief Datastelsel (FDS) staan de volgende elementen uit het hiervoor
genoemde beleidsdoel centraal:

- Overheid
- Data
- Zowel binnen als tussen domeinen
- Vertrouwen

Dit maakt het FDS tot een publiek (= overheid) stelsel dat het datapotentieel van verschillende
sectorale datastelsels (= binnen en tussen domeinen) in overeenstemming met publieke waarden (=
vertrouwen) voor publieke doelen (= maatschappelijke opgaven) beschikbaar maakt. Datastelsel wordt
in dit verband als synoniem beschouwd voor de termen “data space” en “data ecosysteem”.  
Het FDS gaat uit van het ontsluiten en betekenisvol verbinden van decentrale databronnen zodat deze
volgens het principe van “[data bij de bron](../data-bij-de-bron)” op een verantwoorde manier voor
overheidsdienstverlening kunnen worden toegepast. Het principe heeft een sterke relatie met
datasoevereiniteit. Door gebruikers, bijvoorbeeld via API’s, rechtstreeks toegang tot de data te
geven in plaats van ze als kopie uit te wisselen, houden de partijen die data via het stelsel
aanbieden zicht op deze data. Ze weten welke partijen toegang tot de data hebben en waarvoor ze deze
willen gebruiken. De op het stelsel aangesloten data-afnemende organisaties kunnen op hun beurt
rechtstreeks de aangeboden data consumeren en deze naar eigen behoefte met andere data combineren.
Dit uiteraard voor zover ze daartoe geautoriseerd zijn en dit ook informatiekundig* en ethisch
verantwoord is.

> *Hier moet een verwijzing komen naar een paragrafen over informatiekundige kern (juiste sleutels)
> en het aansluiten van de context van inwinning en gebruik.

 Het FDS is dus een datastelsel dat datadeel relaties van data-aanbieder rechtstreeks naar
 data-afnemer  faciliteert. Hiermee verschilt het FDS van datadeeloplossingen waarin data van
 verschillende aanbieders fysiek, als kopie, wordt samengebracht in voorzieningen zoals “data
 warehouses” en “data lakes” en waarbij een derde, intermediaire, partij tussen de data-aanbieders
 en de data-afnemers is geplaatst.

## Waarin verschilt het FDS van andere datastelsels

Een bijzonder kenmerk van het Federatief datastelsel is dat het andere datastelsels verbindt. Dit
maakt het mogelijk om data uit verschillende datadeelstelsels bij maatschappelijke opgaven te
gebruiken. Daarvoor vult  FDS de generieke digitale infrastructuur van de overheid aan met
technische- en organisatorische stelselfuncties die het mogelijk maken om op een verantwoorde manier
het totale overheidsdatapotentieel beter te benutten. Om dit beheersbaar te houden geldt daarbij het
principe: decentraal wat kan, centraal wat moet. De functies van het FDS blijven beperkt tot wat
noodzakelijk is om effectief en efficiënt bovensectoraal data “vanuit de bron” te delen, oftewel om
duurzaam technische, semantische en juridische interoperabiliteit tussen onder het FDS-regime
aangeboden datasets te realiseren. Het FDS biedt daavoor meer dan interoperabiliteitsafspraken en –
standaaden. Het FDS-afsprakenkader bevat ook afspraken over de minimaal te realiseren inhoudelijke
kwaliteit van (meta)data en datadiensten. Dit geheel zorgt ervoor dat afnemers goed kunnen bepalen
of ze voor hun dienstverlening op een een FDS-compliant databron kunnen bouwen.  

Naast het sectoraal/domeinoverstijgende aspect maakt het publieke karakter het FDS bijzonder. Bij
het datadelen binnen de FDS-context moeten de continuïteit van de publieke taakuitvoering en het in
acht nemen van publieke waarden zijn verzekerd. Daarvoor gelden specifieke normen zoals de algemene
beginselen voor behoorljk bestuur. Het naleven daarvan moet niet alleen zijn geborgd, maar voor het
maatschappelijk vertrouwen in de werking van het FDS, moet dit ook kunnen worden aangetoond. Het FDS
vereist daarom niet alleen dat de toepassingsmogelijkheden en de kwaliteit van de data duidelijk
zijn, maar ook dat transparant kan worden gemaakt wat er in werkelijkheid is gebeurd: welke data,
met wie, voor welk doel, wanneer is gedeeld. Het kunnen borgen van de publieke waarden en het belang
van de continuïteit van de publieke dienstverlening, zal er bovendien toe leiden dat vanuit het FDS
specifieke aansluitvoorwaarden worden opgelegd die bepaalde datasets, al dan niet tijdelijk,
uitsluiten. Zo is er vooralsnog vanuit de Interbestuurlijke Datastrategie voor gekozen om alleen
organisaties, datasets en datadiensten tot het FDS toe te laten die verbonden zijn met een
wettelijke taak. Dit betekent dat het FDS geen zuiver private datasets[^1] ontsluit. Dit is
overigens geen verbod op delen. Partijen kunnen nog steeds via bilaterale of sectorale
arrangementen, los van het FDS-afsprakenkader, private data (blijven) delen of uitwisselen volgens
een eigen, sectoraal, afsprakenstelsel. Het FDS vervangt geen sectorale afsprakenstelsels, maar
levert het fundament dat het mogelijk maakt om eenvoudiger (want gestandaardiseerd) data met andere
sectoren te delen, zoals geïllustreerd in de hierna volgende figuur:  

![alt text](images/image.png)

Bij het federatieve karakter van het FDS waarbij op basis van gelijkwaardigheid wordt gedeeld, hoort
ook dat er aan de aanbiedende kant eveneens specifieke eisen kunnen worden gesteld. Zo kunnen
aangesloten sectoren en organisaties er voor kiezen om een deel van hun aanbod niet via het FDS te
delen, bijvoorbeeld omdat deze data alleen binnen de sector of het eigen bedrijfsproces waarde
heeft, of omdat het zeer gevoelige data betreft die een hoger beveiligingsniveau nodig heeft dan het
FDS ondersteunt. De gekozen scope van het FDS vormt ook geen permanente, infrastructurele, barrière.
Via aanpassing van de aansluitvoorwaarden kan later alsnog de reikwijdte van het FDS worden
verruimd.

## Wat gaat het FDS bieden

Het federatief datastelsel is nog in opbouw. Het project Realisatie Federatief Datastelsel (R-FDS)
ontwikkelt functies om data-aanbieders en data-afnemers uit verschillende domeinen op een
verantwoorde manier data te laten delen. Deze functies zijn ingebed in een afsprakenstelsel dat waar
nodig is aangevuld met standaarden en voorzieningen. Dit geheel maakt het mogelijk om
data-aanbieders, data-afnemers, datasets en datadiensten te certificeren (een “FDS label”) te geven
waarmee het duidelijk wordt dat deze aan het FDS-afsprakenkader (de FDS aansluitvoorwaarden )
voldoen. Dit is inclusief de afspraken over het toezicht op de naleving. Hiermee wordt een
vertrouwensbasis gecreëerd die het voor partij A in sector X mogelijk maakt om verantwoord data van
partij B in sector Z te gebruiken, zonder dat A daarvoor alle bijzonderheden van partij B en sector
Z hoeft te kennen. Het omgekeerde geld ook. Voor beide volstaat dat ze deel uitmaken van het
overkoepelende FDS en ze er daarom op kunnen vertrouwen dat ze genoeg gemeenschappelijk hebben om
bilateraal data te gaan delen. Voor het eigenlijke delen kunnen ze hun eigen IT-voorzieningen
gebruiken, mits die de conform de daarvoor geldende afspraken de FDS-standaarden hebben
geïmplementeerd. Stesleldeelnemers kunnen daarbij gebruik maken van ondersteunende FDS-voorzieningen
zoals de FDS-stelselcatalogus, maar dit is niet verplicht. De verplichting beperkt zich tot het
naleven van de FDS-afspraken en het implementeren van de daaraan gekoppelde standaarden. Het FDS is
dus geen ICT-systeem of datapakhuis, maar het is ook geen volledig virtueel stelsel. Het is
opgegebouwd uit op afspraken en standaarden gebaseerde organisatorische en technische
stelselfuncties die het volgende mogelijk maken:

### Een overzicht van de inhoud van het FDS

Streven is dat alle elementen van het FDS (alle “assets”): datasets, dataset inhoud, datadiensten,
data-aanbieders, data-afnemers en de onderlinge relaties hiertussen in de vorm van metadata op een
gestandaardiseerde manier worden beschreven. Daarmee ontstaat er inzicht in de inhoud van het FDS en
kan dit overzichtelijk worden weergegeven. Deze in- en overzichtfunctie wordt ook wel
'catalogus'[^2] genoemd . De FDS-catalogus wordt opgebouwd uit de metadata die de stelselpartijen
zelf conform de stelselstandaarden hebben gepubliceerd. De metadata is dus op zichzelf ook weer een
FDS-dataset die als open data volgens de FDS-standaarden voor eenieder wordt ontsloten (open
metadata). Dit maakt de inhoud van het federatief stelsel transparant en biedt partijen de
mogelijkheid deze metadata naar eigen inzicht te gebruiken, bijvoorbeeld door ze te integreren in
een thematische catalogus gericht op een specifieke doelgroep. N.b. dat de metadata open is, wil
uiteraard niet zeggen dat dit ook geldt voor de data en datadiensten die m.b.v. die metadata worden
beschreven.

### Het betekenisvol leggen van datadeelrelaties

Dit betreft de afspraak dat een tot het stelsel toegelaten databron in elk geval via de sleutels uit
de informatiekundige kern[^3]  van het stelsel benaderbaar is. De informatiekundige kern is bedoeld
om de meest voorkomende relaties te kunnen leggen. Het betreft afspraken over het standaardiseren
van de identificatie van natuurlijke personen, niet-natuurlijke personen en locaties. Dit borgt dat
op een informatiekundige betrouwbare manier gegevens kunnen worden gecombineerd (“gekoppeld”) omdat
door het toepassen van gestandaardiseerde id’s zoals het BSN, het KVK-nr en het BAG-id, eenvoudig
kan worden vastgesteld dat het dezelfde burgers, bedrijven of locaties betreft waarvan kenmerken
worden gedeeld. Onderdeel van deze afspraak is ook dat de betreffende datahouder de kwaliteit van de
gelegde koppelingen inzichtelijk maakt en een proces heeft ingericht om deze kwaliteit te bewaken.

Behalve met afspraken over het toepassen van gemeenschappelijke identificerende sleutels ondersteunt
het FDS het leggen van relaties met diensten die de noodzakelijke variëteit in het sleutelgebruik
makkelijker hanteerbaar maken. Dit betreft onder andere het beschikbaar maken van datadiensten voor
het wisselen van sleutels, zoals het omwisselen van identificatie op basis van BSN naar
identificatie op basis van familienaam, huidige voornaam en geboortedatum (en vice versa); diensten
die helpen om administratieve data aan coördinaat gebaseerde geo-data te relateren en eenduidige
afspraken over het omgaan met “restpopulaties”, zoals buitenlandse vastgoedeigenaren die niet in
BRP/RNI zijn opgenomen en situaties dat de informatiekundige kern opgenomen sleutels niet kunnen
worden toegepast omdat de gebruikscontext teveel verschilt.

### Het betrouwbaar identificeren van de stelseldeelnemers

Om de legitimiteit van datadeelrelaties te kunnen valideren, moeten de partijen in de relatie, de
data-aanbieder en de data-afnemer en de door hen gebruikte datadiensten, op een betrouwbare manier
geïdentificeerd kunnen worden. Binnen het FDS geldt de afspraak dat de identificatie beperkt blijft
tot het niveau van organisatie. Voor de implementatie wordt aangesloten op afspraken, standaarden en
voorzieningen die vanuit het GDI-domein Toegang beschikbaar worden gesteld. Deze worden zo nodig
aangevuld met afspraken die specifiek op datadelen vanuit de bron betrekking hebben.

### Het autoriseren van datadeelrelaties

Dat een relatie informatiekundig betrouwbaar *kan* worden gelegd, betekent niet dat deze ook *mag*
worden gelegd. Binnen het FDS geldt de afspraak dat een data-aanbieder alleen data aan een
data-afnemer beschikbaar stelt als hij heeft vastgesteld dat deze daartoe gerechtigd is. Om dit
geautomatiseerd te kunnen vaststellen, moet de gestelde datavraag op aan een aantal op FDS-niveau
vastgestelde voorwaarden voldoen. Deze, nog uit te werken, voorwaarden hebben zowel betrekking op de
vorm van de vraag (de technische standaard) als de inhoud (de inhoudelijke standaard). Deze
voorwaarden maken het mogelijk in samenhang met de hierna genoemde registratiefunctie (het 'loggen')
achteraf te kunnen controleren of het gegevensdelen tussen de verschillende stelseldeelnemers
volgens de regels is verlopen en burgers inzicht te geven in wie waarvoor zijn gegevens binnen het
stelsel heeft gebruikt.

### Het legitimeren van nieuwe datadeelrelaties

Voorwaarden voor datadeling zijn de uitkomst van een proces waarbij de belangen voor het wel en niet
delen zorgvuldig tegen elkaar zijn afgewogen. Deze afweging is tijdgebonden. Datadeling die eerst
noodzakelijk was, kan door nieuwe technische mogelijkheden overbodig worden als volstaan kan worden
met alleen een antwoord op een specifieke vraag (dataminimalisatie). Maar het is ook mogelijk dat
waar er eerst geen zwaarwegend belang voor datadeling was, dat er nu, al dan niet tijdelijk, wel is.
Het FDS voorziet in (organisatorische) functies om dit afwegingsproces te faciliteren en de
resultaten daarvan op een gestandaardiseerde manier in de ‘verwerkingenregisters’ van de
stelseldeelnemers vast te leggen. Deze stelselfunctie wordt de Poortwachter[^4] genoemd .

### Het registreren (loggen) van datadeling

Dit betreft drie hoofdafspraken die bedoeld zijn om transparant te maken wat er werkelijk is gedeeld
en de legitimiteit daarvan te kunnen controleren:

- De afspraak dat data-aanbieders op een gestandaardiseerde manier registreren (loggen) welke data,
  wanneer, aan welke organisatie, op basis van welke geclaimde grondslag, op welke manier (via welke
  dienst) beschikbaar is gesteld.  
- De afspraak dat data-afnemers eveneens op een gestandaardiseerde manier loggen welke data,
  wanneer, van welke organisatie, op basis van welke grondslag, of welke manier (via welke dienst)
  is afgenomen. Hierbij geldt voor data die niet als “open data” is geclassificeerd binnen het FDS
  de aanvullende afspraak dat afnemende organisaties ook op persoonsniveau inzichtelijk moeten
  kunnen maken wie de betreffende informatie heeft gebruikt.  
- De afspraak dat bij een stelseldatabron die is opgebouwd uit data uit andere stelseldatabronnen er
 in de logging ook de relatie naar die oorsprong kan worden gelegd zodat er een sluitende “audit
 trail” van het datagebruik kan worden opgebouwd Het transparant maken van het datagebruik

Het FDS voorziet in afspraken en standaarden die het mogelijk maken om de op de stelselmanier
gelogde datadelingen te aggregeren tot een virtuele dataset die op zich ook weer als een
stelseldatabron ontsloten kan worden voor monitoring ter ondersteuning van de stelselgovernance
(bijvoorbeeld in geval van doorbelasting), het stelseltoezicht, het stelselbeheer en de
stelselbeveiliging. Daarnaast maakt deze gestandaardiseerde wijze van logging het mogelijk om op
geaggregeerd en individueel niveau aan burgers, bedrijven en “de maatschappij” het datagebruik
inzichtelijk te maken en het opbouwen en bijhouden van de door de AVG voorgeschreven
‘verwerkingenregisters’ deels te automatiseren.  

### Aansluitondersteuning, validatie en certificering

Data-aanbieders en data-afnemers sluiten aan op het FDS doordat ze zich aanmelden en daarbij zelf
verklaren dat ze zich conformeren aan het FDS-afsprakenkader en dat ze hierop ook kunnen worden
aangesproken. Bij de data-aanbieders geldt deze verklaring ook voor de kwaliteit van de binnen de
FDS aangeboden datasets en datadiensten. Het aangesloten zijn komt tot uiting in het verkrijgen van
een FDS-label. Dit label is onderdeel van de metadata en kent verschillende, nog uit te werken,
kwaliteitsklassen (rankings). Vertrekpunt is dat partijen zelf hun niveau van stelselconformiteit
beschrijven en verifiëren (self description en self assessment). Het stelsel biedt daarvoor kaders,
referentietoepassingen en validators. Het aansluiten zelf wordt ondersteund met testomgevingen,
testdata en testtool en met kennis (handleidingen, best practices, faq’s etc.) geleverd vanuit het
FDS Expertisecentrum .

### Gebruikersondersteuning

Het FDS kent 3 nog in te vullen en in te richten functies om de stelseldeelnemers te ondersteunen:

1. De Marktmeester  die ondersteunt bij het matchen van vraag en aanbod.
2. De Expertisefunctie, die kennis levert over de inhoud en het toepassen van de FDS-afspraken,
   standaarden en voorzieningen.
3. De Helpdesk die ondersteuning biedt voor partijen die een “stelselprobleem” hebben of die
   gebruikers op weg helpt die niet zelfstandig de weg kunnen vinden naar de partij die voor de
   betrokken data- en of datadienst verantwoordelijk is.

### Besturing van het FDS

Er is een behoefte aan regie en hiervoor is in het ontwerp van het stelsel de functie van Regisseur
voorzien. Hoe deze functie in een federatief stelsel effectief kan worden ingevuld, moet nog worden
uitgezocht. Een belangrijk element daarbij wordt het scherp krijgen van de verhouding tussen
doorzettingsmacht op stelselniveau en de ministeriële verantwoordelijkheid voor de sectoren waaruit
via het FDS databronnen worden ontsloten. Onderdeel van de uitwerking van de stelselregie is ook het
zorgen voor een redelijk en praktisch hanteerbaar financieringsarrangement voor het gebruik, het
beheer en de doorontwikkeling van het FDS.

### Beheer en exploitatie van het FDS

Het strategisch, tactisch en operationeel beheer van de componenten van het FDS zal goed geregeld
moeten worden, maar de exacte invulling moet nog worden uitgewerkt. Op strategisch en deels tactisch
niveau is het beheer van het stelsel onderdeel van de regisseurrol, maar  op operationeel niveau zal
er ook beheer van de in projectfase ontwikkelde stelselfuncties nodig zijn.

### Toezicht en handhaving

Het ontwerp van het FDS kent de functie Toezichthouder. Van deze functie staat vast dat hij nodig
is, maar ook hier geldt dat de invulling nog uitgewerkt moet worden Een belangrijk uitzoekpunt is of
er behoefte is aan een daadkrachtige onafhankelijke FDS-toezichthouder: een toezichthouder die het
instrumentarium heeft om zelf een goed beeld van de naleving van afspraken te krijgen en in te
grijpen als dit niet naar behoren gebeurt. Dergelijk “toezicht met tanden” is mogelijk noodzakelijk
voor het borgen van de publieke waarden die het FDS moet dienen.

### Het faciliteren van koppelingen tussen aangesloten stelsels /dataspaces

Het beeld bestaat dat het FDS functies moet gaan leveren om het leggen van deze koppelingen te
vereenvoudigen, maar welke dat zijn moet nog worden uitgewerkt. Aandachtspunten zijn onder andere
verschillende standaarden voor authenticatie en autorsatie binnen te koppelen datastelsels en het
gebruik van verschillende identificerende nummers.Daarnaast zijn er mogelijk functies nodig om de
kwaliteit en het gebruik van deze koppelingen te kunnen monitoren.

### Conformiteit met algemene EU dataregelgeving

Deelnemers aan het FDS kunnen ervan uitgaan dat ze met het implementeren van de FDS-afspraken en
standaarden ook voldoen aan de algemene datadeeleisen die vanuit EU-regelgeving worden opgelegd. Ze
kunnen zich dus focussen op het uitwerken en implementeren van de EU-dataregelgeving die specifiek
voor hun sector/domein geldt. Deze zekerheid bieden is een doel bij de realisatie van het FDS, maar
het is nog niet duidelijk of dit een afzonderlijke stelselfunctie vergt of dat het een aspect is bij
de inrichting van de andere FDS-functies.

[^1]: Hier moet een verwijzing naar de two pager private datasets komen.
[^2]: Hier moet een verwijzing naar de two pager catalogus komen
[^3]: Hier moet een link naar de uitleg van de informatiekundige kern komen
[^4]: [Stelselfunctie Poortwachter](../stelselfuncties#poortwachter)
