---
title: FSC
capabilities: [Identiteit, Toegang, Veiligheid, Verantwoording]
description: >
  Federatieve Service Connectiviteit (FSC) Standaard
status: draft
date: 2024-05-15
author: R-FDS Team & Team FSC
version: v0.1
---
{{% imgproc "**capabilities*.*" Resize "x200" /%}}

## Beschrijving

Federatieve Service Connectiviteit, **FSC**, biedt verbetering én optimalisatie in het beheer van
technische identiteiten van overheden. Daarnaast biedt FSC connectiviteit in koppelingen tussen
overheden met verbeterde beveiliging en mogelijkheden voor federatieve verbindingen. In de
beveiliging zitten mogelijkheden voor autorisatie en verwerkingenlogging.

Meer info is te vinden op de website van
[CommonGround/FSC](https://commonground.nl/cms/view/736309a1-739a-47fc-abfd-67e71f1d9e59/consultatie-fsc)

![FSC Consumer Provider](images/fsc-consumer-provider.png)

## Architectuurlagen

### Grondslagenlaag

Hier vind je wetten en drivers.

Componenttypen die we hier verwachten:

- Doel/Driver
- Principe
- Requirement
- Afspraak/Standaard (Wet/Overeenkomst)

### Organisatorische laag

De organisatorische componenten vind je hier.

Componenttypen die we hier verwachten:

- (Actor)
- Rol
- Functie
- Service
- Afspraak/Standaard
- (Contract)

### Informatielaag

Componenttypen die we hier verwachten:

- Metadata-object
- Metadata-object-relatie
- Afspraak/Standaard

### Applicatielaag

Componenttypen die we hier verwachten:

- Functie (Technische stelselfuncties)
- Applicatieservice
- Afspraak/Standaard

### Technologielaag

Componenttypen die we hier verwachten:

- Node
- Netwerk
- Afspraak/Standaard
