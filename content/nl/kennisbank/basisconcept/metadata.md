---
title: Metadata
weight: 8
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: Op hoofdlijnen de rol van metadata binnen FDS.
---

Om data effectief te kunnen gebruiken moet duidelijk zijn in welke context en met welk doel de data
is ontstaan en wat de betekenis ervan is. Om te kunnen weten en begrijpen wat bepaalde data
betekent, beschrijven we deze met metadata.

## Wat is metadata?

De NORA definieert metadata als: ’Gegevens die context, inhoud, structuur en vorm van informatie en
het beheer ervan door de tijd heen beschrijven.‘ Metadata vormt een essentieel fundament onder de
werking van het Federatief Datastelsel. De metadata vertelt o.a.:

- welke data binnen het stelsel beschikbaar zijn,
- wat de betekenis (semantiek) van die data is,
- wat de grondslag is voor het verzamelen ervan,
- voor welke context de data oorspronkelijk zijn bedoeld,
- welke partijen daar bij betrokken zijn,
- wat de rollen en verantwoordelijkheden zijn van de betrokken partijen,
- hoe de data is vormgegeven (syntax),
- hoe de data beschikbaar komt (API's bijv.),
- wat de kwaliteit van de data en datadiensten is (denk aan juistheid, actualiteit, etc.)
- voor wie de data beschikbaar is en onder welke condities.

## Waarom metadata?

De metadata geeft potentiële afnemers inzicht in wat binnen het stelsel beschikbaar is, voor wie en
onder welke condities.

## Federatieve stelselcatalogus

De metadata komt beschikbaar via de ‘federatieve stelselcatalogus’. Dit is een functionaliteit die
voorziet in de mogelijkheid om data-assets van het FDS (i.c. data, datadiensten, data-aanbieders,
data-afnemers met hun onderlinge relaties) in de vorm van metadata op gestandaardiseerde wijze te
beschrijven. Stelselpartijen beschrijven de metadata zelf, conform de stelselstandaarden. De
stelselfuncties voorzien in validatie van de metadata, waarna de betreffende stelselpartij deze
beschikbaar stelt voor ontsluiting door deze te publiceren.

Door het geheel van de beschrijvingen in samenhang ontsluitbaar en presentabel te maken, wordt het
inzichtelijk voor belangstellenden wat FDS kan bieden. De metadata van de federatieve catalogus is
daarmee op zichzelf FDS-data die als open data volgens de FDS-standaarden voor belangstellenden
wordt ontsloten.
