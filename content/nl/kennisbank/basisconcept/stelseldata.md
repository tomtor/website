---
title: Stelseldata
weight: 6
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: >
    Op hoofdlijnen beschrijving van data als begrip en de eisen die het stelsel aan het data-
    aanbod verbindt.
---

‘Data’ is afkomstig uit het Latijn en betekent zoveel als ‘gegevens’ of ‘feiten’. Het Latijnse
enkelvoud is ‘datum’. De verwarring die hierbij kan ontstaan met het welbekende tijdsbegrip is
evident. We gebruiken daarom de term ‘data’ voor zowel het enkelvoud als voor het meervoud van
‘gegeven’. Uit de context moet blijken of het om het meervoud gaat of om het enkelvoud.

Om data als begrip te omschrijven gebruiken we de definitie zoals omschreven in <a
href="https://eur-lex.europa.eu/legal-content/NL/TXT/HTML/?uri=CELEX:32022R0868#d1e969-1-1"
target="_blank">artikel 2 lid 1 van de Europese Data Governance Act</a>:

***Elke digitale weergave van handelingen, feiten of informatie en elke compilatie van dergelijke
handelingen, feiten of informatie, ook in de vorm van geluidsopnames of visuele of audiovisuele
opnames.***

FDS stelt eisen aan de data die in het stelsel worden opgenomen. Deze eisen hebben invloed op de
inhoud van de datadiensten waarmee het data-aanbod in het stelsel voor de data-afnemers wordt
ontsloten. De belangrijkste eisen die FDS stelt aan data zijn:

- De data hebben een [wettelijke grondslag](#wettelijke-grondslag)
- De data mag [meervoudig gebruikt worden](#meervoudig-bruikbaar)
- De data zijn adequaat beschreven
- De data zijn in betekenisvolle samenhang ontsluitbaar

## Wettelijke grondslag

De datastrategie is gericht op (het beter) dienen van het publiek belang, door data verantwoord voor
maatschappelijke opgaven te benutten. Dit vereist data waarvan verzekerd is dat deze juridisch en
ethisch verantwoord kan worden gebruikt in publiek belang. Hiermee borgt FDS de publieke waarden en
biedt tegelijk ook de data-afnemers het vertrouwen en de zekerheid dat zij hun publieke
dienstverlening op data uit het stelsel kunnen en mogen baseren. Data die via FDS wordt gedeeld moet
daarom voor zowel het aanbieden als het afnemen en toepassen ervan, een grondslag hebben. Die
grondslag moet te herleiden zijn naar Nederlandse en/of Europese wet- en regelgeving.

Zo vinden het data-aanbod en het datagebruik uit de Basisregistratie Personen hun grondslag in de <a
href="https://wetten.overheid.nl/BWBR0033715/2023-10-01#Hoofdstuk1" target="_blank">Wet
basisregistratie personen</a>. De Nederlandse Overheid Referentie Architectuur (NORA) <a
href="https://www.noraonline.nl/wiki/Alle_sectorregistraties" target="_blank">beschrijft een groot
aantal (sector)registraties</a> en benoemt daarbij hun wettelijke grondslag. Bijvoorbeeld het <a
href="https://www.noraonline.nl/wiki/Diplomaregister" target="_blank">Diplomaregister</a>.

De wettelijke grondslag moet rechtvaardigen dat (bepaalde) data meervoudig kan worden gebruikt met
andere doelstellingen en in een andere context, dan waarvoor de data oorspronkelijk is ingewonnen.
Als er geen grondslag is, kan de data pas in het stelsel worden aangeboden als deze is gecreëerd.
Ook afnemers kunnen alleen tot het stelsel toetreden als zij een wettelijke grondslag hebben voor
het benutten van het stelselaanbod, i.c. als zij een wettelijke taak vervullen. De FDS
stelselfuncties ondersteunen bij het creëren van het vereiste wettelijke kader en het borgen dat het
aanbieden en afnamen in overeenstemming met de van toepassing zijnde wettelijke grondslagen
plaatsvindt.

## Meervoudig bruikbaar

Data wordt ingewonnen en/of bewerkt met een reden (doel) in een bepaalde context. Maar data kan ook
bruikbaar zijn voor andere doelen in andere contexten. Zo is <a
href="https://www.rvo.nl/onderwerpen/identificatie-en-registratie-dieren"
target="_blank">Identificatie en registratie van dieren</a> nodig om snel te kunnen handelen bij
uitbraak van dierziektes die een gevaar voor de volksgezondheid vormen. Dergelijke data kan echter
ook nuttig zijn voor inzicht in concentraties van diersoorten die van invloed (kunnen) zijn op
milieuomstandigheden. Bij hergebruik van data zal de gebruikscontext altijd anders zijn dan de
gebruikscontext waarvoor de data oorspronkelijk is ingewonnen. Vooraf is een dergelijke nieuwe
context niet bekend. Om verantwoord hergebruik van data in een andere context te kunnen bepalen, is
minimaal inzicht vereist in:

- Betekenis, context en doel van de her te gebruiken data;
- Condities voor (her)gebruik;
- Context en doel van het nieuwe gebruik.

## Data zijn adequaat beschreven

De beschrijvingen van data en datadiensten moeten inzicht geven in de mogelijke herbruikbaarheid van
de betreffende data. Hiervoor moet bijvoorbeeld duidelijk zijn wat de betekenis is van de data en
waarvoor deze oorspronkelijk is ingezameld en gebruikt. Lees voor meer uitleg verder op pagina
[Metadata](../metadata/).

## Data zijn in betekenisvolle samenhang ontsluitbaar

Voor het betekenisvol combineren (of compileren) van data moet deze voorzien zijn van uniek
identificerende kenmerken, ook wel sleuteldata genoemd. Relaties tussen data worden gelegd via deze
sleuteldata. Met sector-overstijgend data delen als uitgangspunt is het noodzakelijk dat data uit
verschillende sectoren is voorzien van overeenkomstige sleuteldata. FDS onderhoudt een
afsprakenkader dat beschrijft welke sleuteldata minimaal aanwezig moet zijn, om te kunnen koppelen
met andere data. Dit afsprakenkader is bekend onder de naam ‘Informatiekundige Kern’ (IK). De
relaties tussen sectoren blijken vooral gelegd te kunnen worden op basis van ‘wie’ en/of ‘waar’,
ofwel de identificerend kenmerken van natuurlijke personen, organisaties en locaties. Binnen
sectoren zijn de onderwerpen en objecten waar de sector over gaat belangrijke factoren voor het
leggen van relaties. Denk aan voertuigen in de mobiliteitssector, studieprogramma’s in de
onderwijssector, etc. De IK bestaat uit afspraken over de identificatie van natuurlijke personen,
niet-natuurlijke personen en locaties. Dit betreft de identificerende eigenschappen van BRP, HR en
BAG. Doordat data-aanbieders in FDS conformeren aan de IK-afspraken kunnen ze datadiensten aanbieden
die op informatiekundig betrouwbare manier data combineren. Als twee verschillende datasets dezelfde
unieke sleutel gebruiken (bijv. BSN 999.99.999 van een persoon), is het simpel vast te stellen dat
het gaat om data over dezelfde persoon. De bij deze sleutel behorende datakenmerken uit beide
datasets zijn dan eenvoudig te combineren tot een samengesteld beeld over de betreffende persoon.
Naast afspraken over standaard identificerende sleutels voorziet FDS ook in afspraken over
alternatieve sleutels. Stel dat een data-aanbieder niet mag of kan beschikken over een BSN, maar de
data die hij wil aanbieden leent zich wel voor het leggen van relaties op persoonsniveau. In dat
geval kan wellicht een alternatieve sleutel worden toegepast. Denk aan postcode en huisnummer voor
een adres, of geboortedatum, voornamen en achternamen voor een persoon. Bij gebruik van alternatieve
sleutels kan altijd een risico bestaan dat een sleutel niet 100% uniek is. Ook over het mitigeren
van dit risico zijn afspraken nodig, evenals over hoe te handelen bij eventuele maatschappelijke
gevolgen bij ongewenst koppeling van data. De IK zal ook nog afspraken uitwerken over de invulling
van ondersteunende functies. Dergelijke functies moeten de implementatie van de afspraken
eenvoudiger maken. Te denken valt aan:

- Opzoeken van sleutels;
- Wisselen van sleutels naar alternatieve sleutels. Denk aan het omwisselen van identificatie op
  basis van KVK-nummer naar identificatie op basis van RSIN voor een niet-natuurlijk persoon.
- Gebruik van pseudoniemen bij persoonsgegevens.
