--- 
title: Doel en aanpak
weight: 2
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: >
   Het doel van FDS en de aanpak om dat doel te bereiken. Kort wordt ook ingegaan op de
   interpretatie van het begrip 'federatief'.
---
Het Federatief Datastelsel is geënt op het volgende beleidsdoel uit de interbestuurlijke
datastrategie:

> ‘Als overheid benutten we het volle potentieel van data bij maatschappelijke opgaven, zowel binnen
> als tussen domeinen, op een ambitieuze manier die vertrouwen wekt.’

Bron: <a
href="https://realisatieibds.nl/files/view/1c9729a6-70a9-4bda-a245-49c1d6d63adb/meerjarenaanpak-interbestuurlijke-datastrategie6maart2023.pdf"
target="_blank">Meerjarenaanpak Interbestuurlijke Datastrategie</a>.

Het hoofddoel van FDS is om het voornoemde beleidsdoel mogelijk te maken. Daarvoor faciliteert FDS
dat Nederlandse overheidsorganisaties op eenvoudige en verantwoorde wijze hun data met elkaar kunnen
delen ten bate van maatschappelijke vraagstukken en publieke dienstverlening. Hierdoor kan de
Nederlandse overheid meer maatschappelijke waarde genereren uit de (data)middelen waarover ze op
grond van haar wettelijke taken beschikt.

De hoofdoelstelling van FDS wordt in de realisatie- en implementatieplannen verder verdiept.

## Federatief

Een federatie is feitelijk een samenwerking tussen de leden op basis van collectieve afspraken. De
leden zijn en blijven verantwoordelijk voor de kwaliteit van de data die ze inbrengen in het
stelsel. Ze stellen binnen die verantwoordelijkheid hun data beschikbaar voor hergebruik en
conformeren daarbij aan de collectieve stelselafspraken en relevante wetgeving. Ook kunnen ze zo
nodig aanvullende afspraken maken, waaraan zijzelf en de andere betrokkenen zich dienen te houden.
[Stelselbekwaamheden](../bekwaamheden/) zoals ‘Governance’ moeten dit waarborgen.

Het waarborgen van het functioneren van een federatie is in beginsel de verantwoordelijkheid van de
leden, doordat zij zich ieder afzonderlijk conformeren aan de collectieve afspraken. Een centraal
niveau is er als waarborg voor collectiviteit en de daarvoor essentiële functies die op het
decentrale niveau moeilijk zijn in te richten en/of daarvoor niet efficiënt zijn.

## Aanpak project ‘Realisatie Federatief Datastelsel’

‘Het Federatief Datastelsel’ is een afsprakenstelsel in opbouw. Het project Realisatie Federatief
Datastelsel ontwikkelt functies om aanbieders en afnemers van data uit alle beleidsdomeinen op een
juridisch en ethisch verantwoorde manier data te laten delen. De functies zijn ingebed in een
afsprakenstelsel dat waar nodig is verankerd in wetgeving. Het afsprakenstelsel kan verder worden
aangevuld met concrete standaarden en voorzieningen. Data, data-aanbieders, data-afnemers en en
datadiensten in het stelsel, kunnen zo worden voorzien van een ‘FDS-label’ dat duidelijk maakt dat
zij voldoen aan het FDS-afsprakenkader (waaronder bijvoorbeeld FDS aansluitvoorwaarden).
Stelselfuncties zoals ‘toezicht houden’ zorgen ervoor dat deelnemers structureel conformeren, ook na
eventuele wijzigingen binnen het stelsel. Dit alles levert een vertrouwensbasis waarbinnen
deelnemers aan het stelsel verantwoord data kunnen delen met andere deelnemers.

Om data te delen maken stelseldeelnemers in de rol van data-aanbieder deze data binnen het stelsel
beschikbaar. Dat gebeurt door het leveren van datadiensten. De data-aanbieder publiceert de metadata
over de data en diensten die hij beschikbaar stelt, zodat een (potentiële) data-afnemer kan bepalen
of dit voor hem bruikbaar is.

Voor het feitelijke datadelen zorgen de deelnemers zelf dat ze kunnen beschikken over de benodigde
infrastructurele en functionele voorzieningen. Deze voorzieningen zijn zodanig ontwikkeld en
ingericht dat ze conformeren aan de relevante stelselafspraken en -standaarden. Gebruik van
eventuele generieke FDS-voorzieningen is geen verplichting, tenzij dat voor specifieke situaties
binnen het stelsel is afgesproken en als zodanig vastgelegd in FDS-afsprakenkader.
