---
title: Stelselfuncties
weight: 10
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: Een korte inleiding op stelselfuncties met doorlink naar uitgebreidere beschrijving.
---

Voor een goede werking van het stelsel onderscheiden we een beperkte set aan organisatorische en
technische stelselfuncties. Deze functies zorgen ervoor dat gebruikers het stelsel als een geheel
ervaren en dat ze het data-aanbod in FDS zo eenvoudig mogelijk kunnen gebruiken.

De stelselfuncties worden bij voorkeur decentraal ingevuld op basis van afspraken, standaarden en/of
voorzieningen, waarbij het uitgangspunt 'Afspraken boven standaarden boven voorzieningen' van
toepassing is.

Een uitgebreidere beschrijving is opgenomen op pagina
[Stelselfuncties](/kennisbank/stelselfuncties/).
