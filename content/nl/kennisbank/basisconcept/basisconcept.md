---
title: Het basisconcept
weight: 5
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: De kern van het basisconcept van het Federatief Datastelsel. Aangevuld met een korte toelichting.
---

Het basisconcept voor het Federatief Datastelsel is weergegeven in figuur 4. Dit basisconcept
vervangt de zogenoemde '<a
href="https://realisatieibds.pleio.nl/file/download/21091f37-326c-4383-a60a-e04fb09d5c83/220030-fds-houtskoolschets.pdf"
target="_blank">Houtskoolschets</a>' die is gepubliceerd tijdens de stelseldag in november 2022.

| ![Figuur 4: FDS Basisconcept](../images/fds-basisconcept.png) |
| :-----------------------------------------------------------: |
| _Figuur 4: FDS Basisconcept_                                  |

Het basisconcept vormt het vertrekpunt voor verdere uitwerking tot FDS doelarchitectuur. De concrete
aanpak daarvoor vertrekt uit de zogenoemde basis bekwaamheden. Deze worden gedetailleerd uitgewerkt.
Daaruit leiden we de benodigde processen en functies af en kunnen we de benodigde afspraken,
standaarden en eventuele voorzieningen definiëren.

Het basisconcept kent de volgende componenten:

1. Strategische sturing:
   
   De strategische sturing gaat over het stelsel. Dit betreft vooral een
   Plan-Do-Check-Act-gebaseerde sturing op het maatschappelijk belang en de publieke waarden en
   daarbinnen op de effectiviteit en efficiency van het datadelen binnen het stelsel. In welke mate
   draagt FDS daadwerkelijk bij aan het beter benutten van data door de Nederlandse overheid, bij
   maatschappelijke opgaven en individuele dienstverlening conform de menselijke maat.

   De tactische en operationele sturing zijn bekwaamheden van het stelsel die in vervolg op dit
   basisconcept worden uitgewerkt.

2. Data-aanbod:
   
   Het data-aanbod krijgt gestalte doordat ***Data*** in het stelsel wordt ingebracht. De
   ingebrachte data moet (blijvend) voldoen aan de kwalitatieve voorwaarden die in het stelsel zijn
   afgesproken. De aldus  beschikbare data kan door een ***Data-aanbieder*** worden aangeboden in de
   vorm van datadiensten. Het data-aanbod wordt verder toegelicht op de pagina
   '[stelselrollen](../stelselrollen/#data-aanbieder)'.

3. Data-vraag:
   
   De vraag naar stelseldata wordt ingevuld doordat een ***Data-afnemer*** een of meer datadiensten
   afneemt van een of meer data-aanbieders. De data-vraag wordt verder toegelicht op de pagina
   '[stelselrollen](../stelselrollen/#data-afnemer).

4. Stelselmechanismen
   
   FDS heeft mechanismen om te borgen dat data betrouwbaar en vertrouwd in het stelsel kan worden
   gedeeld en dat dit juridisch is toegestaan en ethisch is verantwoord. Op hoofdlijnen betreft dat
   '[bekwaamheden (of capabilities)](../bekwaamheden/)' die het stelsel moet hebben, welke worden
   ingevuld door daartoe ingerichte '[stelselfuncties](../stelselfuncties/)'.
