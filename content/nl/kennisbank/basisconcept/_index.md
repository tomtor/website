---
title: Basisconcept
weight: 20
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: Het basisconcept van het Federatief Datastelsel.
---

Complexe maatschappelijke opgaven vragen steeds vaker om samenwerking binnen en tussen sectoren, in
ketens en netwerken waarin gegevens gezamenlijk worden gebruikt. De <a
href="https://realisatieibds.pleio.nl/" target="_blank">Interbestuurlijke Datastrategie</a>
beschrijft een strategie gericht op een beter gebruik van het datapotentieel van en door de
Nederlandse overheid. Niet alleen beter gebruik, maar ook juridisch en ethisch verantwoord gebruik.
Een effectief functionerend Federatief Datastelsel wordt daarbij gezien als een essentieel middel op
nationaal niveau, ingebed in Europese kaders.

In het vervolg van dit basisconcept wordt ‘**het Federatief Datastelsel**’ voluit geschreven, of
wordt het verkort aangeduid met alleen het acroniem '**FDS**', dus zonder toevoeging van het
lidwoord ‘**_het_**’.
