---
title: Bekwaamheden
weight: 9
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: Op hoofdlijnen de bekwaamheden waarover het stelsel moet kunnen beschikken.
---

Een bekwaamheid, vaak aangeduid met de Engelse term 'capability', is een vermogen dat iets (bijv.
een systeem) in staat stelt om bepaalde doelstellingen te realiseren. Voor het realiseren van de
doelstellingen van het Federatief Datastelsel, zijn bepaalde bekwaamheden nodig bij de deelnemers in
het stelsel en er zijn algemene bekwaamheden die het stelsel als geheel laten functioneren. In deze
uitwerking ligt de focus bij het laatstgenoemde. De bekwaamheden die een individuele deelnemer nodig
heeft om effectief deel te kunnen nemen beschouwen we als voorwaarden die FDS stelt aan deelname.
Het stellen van die voorwaarden is dan weer een bekwaamheid van het stelsel. FDS benoemt vier basis
bekwaamheden. Deze vormen de kapstok voor het dieper uitwerken naar detailniveaus. Deze bekwaamheden
zijn afgeleid uit het OPEN DEI raamwerk, zoals beschreven in paragraaf 2.3. FDS onderscheidt de
volgende vier basis bekwaamheden:

- Stelsel-governance: Het vermogen van het stelsel om gedragingen en activiteiten van de deelnemende
  partijen te laten conformeren aan de geldende stelselafspraken.
- Interoperabiliteit: Het vermogen van het stelsel om de deelnemende partijen data met elkaar te
  laten delen en/of uit te wisselen.
- Vertrouwen: Het vermogen van het stelsel om vertrouwen te genereren voor verantwoord gebruik van
  data door de deelnemende partijen.
- Data waarde: Het vermogen van het stelsel om data zichtbaar van waarde te laten zijn voor de
  deelnemende partijen.

Elke hoofdbekwaamheid is verder onderverdeeld in deel-bekwaamheden. Hiervoor gebruiken we het model
dat we kort hebben toegelicht bij de beschrijving van [de relatie met de Europese
datastrategie](../context-en-scope/#samenwerking-en-afbakening-binnen-europa). Het model is
weergegeven in figuur 3.

| {{< capabilities-diagram >}}         |
| :----------------------------------: |
| _Figuur 3: FDS capabilities diagram_ |

De bekwaamheden werken we uit tot een of meer **bouwblokken**. De beschreven bouwblokken moeten dus
gerelateerd zijn aan minimaal één bekwaamheid.
