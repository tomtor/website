---
title: Uitgangspunten
weight: 4
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: De uitgangspunten voor het basisconcept van het Federatief Datastelsel.
---

## Grip op data

Een belangrijk uitgangspunt binnen de Europese kaders voor ontwikkelen van dataruimten, is de
zogenoemde ‘data soevereiniteit’. Ruwweg stelt dit uitgangspunt dat deelnemers aan datastelsels
zeggenschap behouden over hun eigen data en het delen ervan met andere deelnemers.

FDS beschouwen we als het Nederlandse publieke datastelsel. Het maatschappelijk belang is leidend
bij het datadelen via FDS. De publieke waarden dienen daarbij dus geborgd te zijn. Die waarden en de
daaruit voortvloeiende beginselen zijn in wet- en regelgeving en in overheidsbeleid verankerd. De
stelseldeelnemers hebben de plicht en verantwoordelijkheid om daarnaar te handelen. De
FDS-stelselmechanismen leveren de deelnemers aan het stelsel voldoende grip op de data om deze op
verantwoorde wijze met elkaar te kunnen delen.

Deze sturing vanuit het overkoepelende maatschappelijke belang, begrenst dus de autonomie van de
individuele stelseldeelnemers. Dit is een belangrijk verschil t.o.v. data-ecosystemen met een
private ‘datamarkt’, waarin deelnemers op basis van economische motieven zelf bepalen met wie en
onder welke condities zij data delen.

## Decentraal wat kan en centraal wat moet

In het federale stelsel conformeren de leden zich aan collectieve afspraken, standaarden en
eventueel voorzieningen. Het uitgangspunt is om voorzieningen alleen verplicht te stellen als dat
functioneel noodzakelijk is om de doelen van FDS te realiseren. Verplicht te gebruiken zijn bepaalde
centrale voorzieningen alleen als dat in wetgeving of de collectieve afspraken zo is opgenomen.

Met collectieve afspraken wordt o.a. bedoeld de richtlijnen, convenanten en standaarden die worden
benoemd in een formeel vast te stellen en te onderhouden FDS-afsprakenkader.

## Afspraken boven (open) standaarden boven voorzieningen

FDS gaat uit van het beginsel 'afspraken gaan boven standaarden en standaarden gaan boven
voorzieningen'. De nadruk ligt niet op realisatie en beheer van (generieke) IT-voorzieningen, maar
op ontwikkelen en toepassen van generieke afsprakenkaders en standaarden.

Het principe houdt in dat voor stelseldoelen die volledig behaald kunnen worden met een of meer
bindende stelselafspraken, geen stelselstandaard of stelselvoorziening wordt ontwikkeld. Als
stelselafspraken alleen niet toereikend zijn, kan (ook) een stelselstandaard worden afgesproken. Pas
als ook een stelselstandaard niet toereikend is, kan een generiek toepasbare stelselvoorziening
worden ontwikkeld.

Voor standaarden hanteert FDS de eis dat dit open standaarden zijn.

## Data bij de bron

Het werken conform ‘data bij de bron’ is een belangrijk beginsel bij de realisatie van de digitale
overheid en daarmee ook voor de realisatie van het Federatief Datastelsel.

De Nederlandse overheid streeft naar goed hergebruik van data. De hergebruikte data wordt door de
afnemer/gebruiker echter nog vaak als kopie opgeslagen. Veelal gebeurt dit omdat het voor bepaalde
functionaliteit of door in capaciteit beperkte infrastructuur, noodzakelijk is. De technologische
vooruitgang zorgt er inmiddels voor dat infrastructuur in veel mindere mate een beperkende factor
is. Daarnaast zijn er technologische paradigma’s die zich richten op het sturen van functionaliteit
naar de brondata om die daar uit te voeren en het resultaat retour te sturen. Dit opent
mogelijkheden om het kopiëren van data te beperken en deze direct bij de bron te gebruiken.

Data bij de bron leidt tot hogere datakwaliteit, meer veiligheid en betere bescherming van privacy;

- Geen fouten door onnodige kopieerslagen;
- Data op één plek is beter te beveiligen en biedt betere bescherming van privacy;
- Makkelijker data kunnen corrigeren, omdat het maar één keer hoeft te gebeuren;
- De overheid kan makkelijker inzicht geven in het gebruik van data;
- De totale hoeveelheid data binnen de overheid vermindert (dataminimalisatie).

Naar verwachting zal distribueren en kopiëren van data om uiteenlopende redenen nooit helemaal
verdwijnen. Het streven is wel om het zoveel mogelijk te beperken. FDS zal daarom voorzien in de
mogelijkheid om gewaarmerkte kopieën in het stelsel onder te brengen. Er zijn twee situaties waarin
een kopie geldt als gewaarmerkt:

1. Als deze aantoonbaar onder controle is van de partij die ook de bron ontsluit;
2. Als deze noodzakelijk is voor het betrouwbaar genereren van afgeleide dataproducten.

Binnen FDS betekent werken volgens ‘data bij de bron’ het rechtstreeks bevragen van de aangewezen
brondata in plaats van het werken met ‘niet gewaarmerkte kopieën’.

Het streven naar het principe ‘data bij de bron’ zal de nodige bereidheid tot verandering vergen van
de betrokken organisaties in het digitale overheidsdomein. Vanuit FDS sturen we o.a. op het geven
van inzicht in de wettelijke grondslag(en) en mogelijke gebruikscontexten van de data in het
stelsel. Verder stimuleren we door actieve promotie het concept en de toepassing ervan, in
samenwerking met het programma ‘Data bij de bron’.

Zie ook [Data bij de bron](/kennisbank/data-bij-de-bron/).

## Sectoroverstijgend datadelen

FDS wil zich in het Nederlandse en Europese landschap van datastelsels (of data spaces) positioneren
als verbindende schakel tussen sectoraal ingerichte stelsels. De focus van FDS is daarmee gericht op
sectoroverstijgende datadeel-relaties, in de veronderstelling dat datadeel-relaties binnen een
sector de focus zijn van de sectorale partijen en/of datastelsels. Dat betekent niet dat
intra-sectorale datadeel-relaties niet mogelijk zijn binnen FDS. Maar FDS houdt geen rekening met
alle eisen en wensen die voor specifieke sectoren van toepassing zijn.

De sectoroverstijgende datadeel-relaties worden ondersteund door adequate beschrijving van de data
(de metadata) en door de zogenoemde [Informatiekundige
Kern](../stelseldata/#data-zijn-in-betekenisvolle-samenhang-ontsluitbaar) (IK).

## Vertrouwensraamwerk

FDS is gericht op het optimaal en vertrouwd kunnen delen van data. Hiervoor is een raamwerk nodig
dat vertrouwen creëert en waarborgt. Een vertrouwensraamwerk bestaande uit functies en mechanismen
gericht op ondersteunen van o.a.:

- veilig en verantwoord gebruik van data,
- waarborgen van privacy,
- rechtmatige en gerechtvaardigde toegang tot data,
- transparantie over aanbod en gebruik van data,
- faciliteren van certificeringen en audits,
- toezicht en audits,
- ...

Deze en andere functies moeten het vertrouwen van de deelnemers in het stelsel vergroten. De
uitwerking van de capability [‘Vertrouwen’](/kennisbank/capabilities/vertrouwen/) zal resulteren in
het vertrouwensraamwerk.
