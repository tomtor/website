---
title: Context en scope
weight: 3
status: proposed
date: 2024-04-30
author: R-FDS Team
version: v0.2
description: > 
   De context waarin het FDS functioneert als onderdeel van de Nederlandse digitale overheid
   enerzijds. Anderzijds hoe we de ontwikkeling van FDS afbakenen t.o.v. andere ontwikkelingen
   binnen de Nederlandse en Europese digitale overheid.
---

## Stelsel van stelsels

Binnen de Nederlandse overheid bestaan diverse initiatieven die zijn gericht op het delen van data
binnen specifieke sectoren. Figuur 1 toont hoe FDS en sectorale dataruimten met elkaar samenhangen.

| ![Figuur 1: FDS als stelsel van stelsels](../images/fds-stelsel-van-stelsels.png) |
| :-------------------------------------------------------------------------------: |
| _Figuur 1: FDS als stelsel van stelsels_                                          |

De sectoren beschikken over de kennis om sectorale data te verbinden, te delen en te gebruiken. FDS
heeft niet de taak en niet het vermogen om specifieke sectorale vraagstukken op te lossen. De voor
FDS ontwikkelde afspraken, standaarden en werkwijzen kunnen wel bijdragen aan sectorale
datastelsels. Maar de focus van FDS ligt vooral op het boven-sectoraal delen van data. Dat wil
zeggen dat data van de ene sector via FDS kan worden gedeeld met een andere sector. FDS vervult als
het ware een brugfunctie. FDS is daarmee het datastelsel dat datastelsels verbindt.

Dit specifieke kenmerk in combinatie met de doelstelling maakt FDS tot een publiek stelsel dat het
datapotentieel van verschillende sectorale datastelsels in overeenstemming met publieke waarden voor
publieke doelen beschikbaar maakt.

## Samenwerking en afbakening binnen Nederland

FDS wordt onderdeel van de digitale overheid (DO) en zal daarin functioneren in relatie met andere
DO-onderdelen. Diverse programma’s en projecten werken (of hebben gewerkt) aan verbetering van de
organisatie en inrichting van de digitale overheid. Programma’s die allen beogen om door adequaat
digitaliseren van overheidstaken en -processen, de Nederlandse maatschappij te voorzien van een
uitstekende en toekomstbestendige overheidsdienstverlening.

Zo werken al deze programma’s evenals ‘Realisatie FDS‘ aan hetzelfde hoofddoel. Goede samenwerking
en afstemming zijn daarbij geboden. Dat gaat niet zonder een duidelijke afbakening van
ontwikkeldoelen en -activiteiten.

Figuur 2 schetst een globaal beeld van de afbakening van de ontwikkeling van FDS-functies ten
opzichte van andere functies voor de digitale overheid.

| ![Figuur 2: Afbakening realisatie FDS](../images/do-fda-afbakening.png) |
| :---------------------------------------------------------------------: |
| _Figuur 2: Afbakening realisatie FDS_                                   |

De figuur toont dat het programma Realisatie FDS zich bezighoudt met overheidsdata beschikbaar maken
voor meervoudig gebruik ten behoeve van enerzijds het kunnen uitwerken van specifieke beleidsopgaven
en anderzijds het uitvoeren van dienstverlening.

Andere DO-programma’s bemoeien zich met de inrichting van digitale overheidsdienstverlening. Onder
andere betreft dit het inzamelen, registreren en beheren van de data die nodig is om de diensten te
verrichten. Voor de inrichting van de digitale overheid maken DO-programma’s onder andere gebruik
van generieke referentiekaders zoals NORA en de Generieke Digitale Infrastructuur (GDI).

De FDS-kaders en de DO-kaders zijn uiteraard niet los van elkaar te zien. Ze zijn beiden onderdeel
van de digitale overheid. Ze overlappen en er is sprake van wederzijdse beïnvloeding. Goede
samenwerking en afstemming tussen de programma’s is derhalve een absolute noodzaak.

## Samenwerking en afbakening binnen Europa

Met de <a href="https://eur-lex.europa.eu/legal-content/NL/TXT/HTML/?uri=CELEX:52020DC0066"
target="_blank">Europese Datastrategie</a> streeft de EU naar veilig en vertrouwd dataverkeer zonder
onnodige belemmeringen tussen publieke en private organisaties binnen Europa als belangrijke
voorwaarde en stimulans voor de eengemaakte Europese markt. Hoewel de EU-ambitie vooral een
economisch motief lijkt te hebben, is het middel waarvoor men kiest vergelijkbaar met het middel
‘FDS’ dat als essentiële systeemfunctie wordt gezien in de Nederlandse Interbestuurlijke
Datastrategie. Onderdeel van EU datastrategie is de ontwikkeling van een aantal dataruimten (data
spaces). Als richtsnoer voor de ontwikkeling van deze dataruimten, verscheen vanuit het door de EU
gefinancierde programma ‘<a href="https://www.opendei.eu/about/" target="_blank">OPEN DEI</a>’ een
instructieve position paper, genaamd ‘<a href="https://design-principles-for-data-spaces.org/"
target="_blank">Design Principles for Data Spaces</a>’. Deze paper beschrijft een aantal principes
en bouwstenen voor de ontwikkeling van dataruimten. De paper is gebaseerd op Europese
referentie-architecturen, in het bijzonder ‘<a
href="https://docs.internationaldataspaces.org/ids-knowledgebase/v/ids-ram-4/"
target="_blank">International Dataspaces Reference Architecture</a>’ en ‘<a
href="https://docs.gaia-x.eu/technical-committee/architecture-document/22.10/"
target="_blank">GAIA-X Reference Architecture</a>’.

Het OPEN DEI-raamwerk is vergelijkbaar met het hiervoor beschreven
[stelsel-van-stelsels-model](#stelsel-van-stelsels). Het raamwerk gaat uit van een generieke laag
met principes en bouwstenen. Bovenop de generieke laag worden de afzonderlijke dataruimten
ontwikkeld, die daarop hun specifieke principes en bouwstenen toepassen. Door consequent en
consistent gebruik van de generieke laag, ontstaat interoperabiliteit tussen de verschillende
dataruimten. Het aldus groeiende stelsel van interoperabele  dataruimten noemt OPEN DEI het Europese
Data Ecosysteem.

FDS kunnen we in deze context beschouwen als het ‘Nederlandse Overheid Data Ecosysteem’. We laten de
FDS-architectuur daarom zo goed mogelijk aansluiten op de architectuur van het ‘EU Data ecosysteem’.
Als kapstok daarvoor gebruiken we het generieke bouwstenenmodel uit het OPEN DEI raamwerk. De
bouwstenen van het raamwerk beschouwen we binnen FDS als bekwaamheden (ook vaak ‘capabilities’
genoemd) waarover het stelsel moet beschikken. Zie Figuur 3.

| {{< capabilities-diagram >}}         |
| :----------------------------------: |
| _Figuur 3: FDS capabilities diagram_ |
