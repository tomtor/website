---
title: Raamwerk
weight: 10
date: 2023-11-06
author: R-FDS Team
tags: [Realisatie IBDS]
description: >
  Het raamwerk welke de structuur vormt voor de [capabilities](/kennisbank/capabilities/)
  en bouwblokken die deze realiseren.
---

Ter ondersteuning van onze [strategie van samenwerken](/docs/strategie-van-samenwerken/)
helpt het om een eenvoudig en begrijpelijk raamwerk (framework) als kapstok te gebruiken. Het
raamwerk ondersteunt vooral de ontwikkeling van het federatief datastelsel door gericht te zijn op
samenwerking en verbinding. Het biedt structuur aan wat we doen, waar we aan werken en biedt
mogelijkheden om verbindingen met onze omgeving te maken.

Een belangrijke verbinding die we willen maken, is naar Europa en Europese ontwikkelingen. Daarom
maken we gebruik van het [Position Paper: Design Principles for Data
Spaces](https://design-principles-for-data-spaces.org/). Hierin heeft de OpenDEI Workforce een
schema van 'building blocks' opgesteld dat door veel Europese initiatieven gebruikt wordt. Ook wij
willen deze gebruiken door deze 'building blocks' als 'capabilities' te zien die het federatief
datastelsel zou moeten ontwikkelen, hebben, zijn. Verdere overwegingen zijn te vinden in het besluit
[00001 Basisstructuur](/besluiten/00001-basisstructuur/). De uitwerking van de
[capabilities](/kennisbank/capabilities/) is te vinden via het menu en/of in de documentatie.

Naast de verbinding naar Europa willen we ook graag verbinding kunnen maken naar andere
architecturen. Dat biedt ook structuur voor veel onderwerpen die 'van verschillend niveau' zijn.
Vrijwel alle architecturen onderkennen daarom verschillende lagen. Zo ook wij. In ons raamwerk
(basisstructuur) zien wij vijf lagen, waarbij de capabilities die hierboven genoemd worden zich
vooral in de bovenste laag bevinden. De vijflagen van ons raamwerk zijn te verbinden met
veelgebruikte lagenmodellen als NORA en Archimate.

De kern van het raamwerk is het capabilities model in combinatie met het vijf lagen model. Dit kan
beschouwd worden als de 'landkaart' waarop alle activiteiten, aspecten, onderwerpen, technologieën
en ontwikkelingen te projecteren zijn. Je weet dan altijd waar het zich bevindt en hoe zich dat
verhoudt tot de rest.

![Basisstructuur Kern](/besluiten/00001-basisstructuur/images/raamwerk.jpg)

Om de verbinding en relaties tot andere lagen modellen te maken, hebben we deze naar NORA en
Archimate al duidelijk ingetekend:

![Basisstructuur Kern](/besluiten/00001-basisstructuur/images/raamwerk-architectuurlagen.jpg)

De kern van het raamwerk bevindt zich in een grotere context welke ... behoorlijk omvangrijk en
indrukwekkend is:

![Basisstructuur Volledig](/besluiten/00001-basisstructuur/images/raamwerk-volledige-context.jpg)

## Links

- doc [Strategie van samenwerken](/docs/strategie-van-samenwerken/)
- doc [Werkomgeving](/docs/werkomgeving/)
- DR [00001 Basisstructuur](/besluiten/00001-basisstructuur/)
- DR [00003 Werkomgeving voor ontwikkeling](/besluiten/00003-werkomgeving-voor-ontwikkeling/)
