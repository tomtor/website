---
title: Data bij de bron
description: Gebruik van transparante en actuele gegevens
status: draft
date: 2024-06-06
author: R-FDS Team
version: v0.1
---
{{% pageinfo color="info" %}}

Deze pagina is **DRAFT**. Het verzamelt de verschillende definities of interpretaties van _Data bij
de bron_. Dit uitgangspunt / principe / begrip wordt veelvuldig gebruikt ... maar niet hetzelfde
geïnterpreteerd en gebruikt 😏

Zie ook:

- [Basisconcept | Uitgangspunten | Data bij de bron](/kennisbank/basisconcept/uitgangspunten/#data-bij-de-bron)
- [Basisconcept | Uitgangspunten | Grip op data](/kennisbank/basisconcept/uitgangspunten/#grip-op-data)

{{% /pageinfo %}}

## Principe: Data direct uit de bron

Het **data bij de bron** principe vormt een basis concept voor de digitalisatie van de
Rijksoverheid, en ook van FDS. We maken gebruik van data zoals deze actueel in de systemen van de
bronhouder beschikbaar zijn, en werken niet met aparte kopieën. Dat zorgt voor een betrouwbare
gegevensvoorziening.

> Onderstaande zou (ook?) in een [decision record](/besluiten/) kunnen staan?

## Context en probleemstelling

Er is veel data binnen de overheid en deze wordt ook nog eens veelvuldig gedeeld en gekopieerd. Het
is onduidelijk waar welke data is, wat de actualiteit en betrouwbaarheid daarvan is, en ook niet
waar persoonsgegevens precies bij en voor gebruikt worden. Hoe kan dit geadresseerd worden?

## Beslissingsfactoren <!-- optional -->

- [Digitale overheid](#digitale-overheid) (over Data bij de bron)
- [Regie op Gegevens](#regie-op-gegevens)
- [NORA principes](#nora-principes)
- FDS & Digilab onderzoek richting Event Sourcing
- … <!-- numbers of drivers can vary -->

## Overwogen opties

- [Digitale overheid](#digitale-overheid)
- [Regie op Gegevens](#regie-op-gegevens)
- [NORA principes](#nora-principes)
- [Grip op data](#grip-op-data)
- … <!-- numbers of options can vary -->

## Besluit

Gekozen oplossing: "Grip op data", omdat het onderliggende problematiek adresseert om data bij de
bron te ondersteunen cq mogelijk te maken en een voorwaarde is om regie op gegevens te kunnen
realiseren.

### Positieve gevolgen <!-- optional -->

- ongecontroleerde kopieën van data moeten worden opgeruimd
- data bij de bron ophalen wordt aangejaagd
- gecontroleerde kopieën van data worden expliciet gemaakt
  - hoe is in onderzoek
- er zal ook grip verkregen moeten worden op historische data (kopieën uit het verleden)
- bij het vastleggen van besluiten dient ook te worden vastgelegd op welke (kwaliteit van) data dit
  besluit gebaseerd is; dit biedt ook mogelijkheden om correcties te (gaan) ondersteunen in geval
  deze data achteraf anders blijkt dan bij het oorspronkelijk besluit

### Negatieve Consequences <!-- optional -->

- snelle en gemakkelijke kopieën van data worden opgeruimd,
  - waardoor processen mogelijk moeilijker en/of langzamer worden
  - wat zwaardere afhankelijkheden tussen organisaties op koppelingen legt
  - data-aanbieders zullen hogere beschikbaarheid en betrouwbaarheid moeten leveren voor hun
    dataservices
- systemen zullen (mogelijk?) anders opgebouwd moeten worden
  - tbv gecontroleerde kopieën
  - tbv historische data

## Pros en Cons van de oplossingen <!-- optional -->

### Digitale overheid

**Data bij de bron** is niet ondubbelzinnig duidelijk. [Data bij de
Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/) wordt als uitgangspunt genoemd bij de
Digitale Overheid:

> Data bij de bron is een belangrijk uitgangspunt voor de digitale transformatie van de Nederlandse
> overheid. Data hoort zo veel mogelijk op één logische plek te staan, bij de eigenaar van die
> gegevens.

Eén logische plek is echter niet zo logisch als een basisregistratie een verzameling is van de
'stukjes' basisregistratie die bij elke gemeente ligt. Dat is namelijk niet één plek. Dat is wél de
eigenaar. Maar wat is er nu belangrijker bij dit uitgangspunt: 'één logische plek' of 'bij de
eigenaar van die gegevens'?

Om hier een uitweg in te vinden, worden extra oplossingen ingebracht zoals Landelijke Voorzieningen.
Op zichzelf zijn deze ook nuttig en van toegevoegde waarde, maar in het kader van data bij de bron
zijn deze ingewikkeld.

- Wat is nu de bron?
- Wie is hier nu eigenaar van?
- De oorspronkelijke gemeente? Die is in ieder geval niet ontslagen van haar verantwoordelijkheid op
  de juistheid van de data
- De uitvoeringsorganisatie? Die is in ieder geval niet direct verantwoordelijkheid voor de
  juistheid, maar wel weer voor de beschikbaarheid e.d.

Dit uitgangspunt levert dan ook weer nieuwe vragen op:

- Wat is 'dé bron'?
- Is dat altijd hetzelfde vanuit elk perspectief? Of mag dat ook anders zijn vanuit data-aanbieder,
  bronhouder en data-afnemer?

**FDS overwegingen**

- Goed, omdat het de oorsprong van data en informatie adresseert
- Goed, omdat het zich richt op waar de verantwoordelijkheid ligt, in ieder geval wat betreft de
  oorsprong van de data
- Slecht, omdat geen uitsluitsel geeft over de nieuwe vragen die het opwerkt
- … <!-- numbers of pros and cons can vary -->

### Programma Data bij de Bron

// TODO

### Regie op Gegevens

[Regie op
gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/) is ook
een programma van de Digitale Overheid. In dit programma is een voorstudie gedaan naar een [kader
voor regie op
gegevens](https://rog.pleio.nl/news/view/f0dc6a48-97e1-4102-a656-72e86a811511/voorstudie-naar-een-kader-voor-regie-op-gegevens):

- Hoofdprincipes
  - _Mens centraal_ 
  - _Digitale autonomie_
  - _Inclusiviteit_
- Inrichtingsprincipes 
  - _Vertrouwen_
  - _Transparantie_
  - _Interoperabiliteit_
  - _Dataminimalisatie_

Al deze principes zijn erop gericht om duidelijkheid en grip te kunnen verschaffen aan personen en
bedrijven daar waar het over hen gaat. Daarvoor is het noodzakelijk om te weten waar welke data is
en wordt gebruikt, zeker in geval van persoonsgegevens.

**FDS overwegingen**

- Goed, omdat het aandacht vraagt voor _verantwoord datagebruik_
- Goed, omdat het (indirect) vraagt naar _grip op data_
- Slecht, omdat het 'slechts' _indirect_ vraagt naar _grip op data_ en niet expliciet; het stelt
  meer een voorwaarde van _grip op data_
- … <!-- numbers of pros and cons can vary -->

### NORA Principes

// TODO samenvatting van [NORA | Informeer bij de bron](https://www.noraonline.nl/wiki/Informeer_bij_de_bron) (NAP10)

Meenemen / overwegen:

- IMP021 [NORA | Bevorder hergebruik van gegevens](https://www.noraonline.nl/wiki/Bevorder_hergebruik_van_gegevens)
- NAP10 [NORA | Neem gegevens als fundament](https://www.noraonline.nl/wiki/Neem_gegevens_als_fundament) (NAP10) ?

### Grip op data

In een federatief datastelsel mag het niet zo zijn dat er 'onbekende' databronnen zijn. Van _alle_
data is bekend waar deze ontstaan is. Van _alle_ data is bekend waar deze vandaan komt. Van _alle_
data is bekend waar deze gebruikt wordt. Op _alle_ data is _grip_. Dus: _grip op data_.

Afgeleide en/of gevolgen daarvan zijn, dat er regie op gegevens gedaan kan worden. En _minder_ data
betekent gemakkelijker en sneller _grip_. Dus zo min mogelijk kopieën van data en als er wel kopieën
zijn, dan zijn deze expliciet inclusief de voorzieningen om actualiteit, traceerbaarheid en gebruik
te volgen.

- Goed, omdat het de kern raakt of voorwaarde is om regie op gegevens te kunnen doen
- Goed, omdat het onbekende en onbeheerde kopieën van data tegengaat, zoals data bij de bron tracht
  te bereiken
- Goed, omdat het een meer onderliggende basis legt zodat data bij de bron beter of anders bereikt
  kan worden
- … <!-- numbers of pros and cons can vary -->
