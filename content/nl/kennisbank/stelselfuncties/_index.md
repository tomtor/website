---
title: Stelselfuncties
weight: 20
status: proposed
date: 2024-04-26
author: R-FDS Team
version: v0.2
description: Organisatorische en technische stelselfuncties
---
Het Federatief Datastelsel is een vertrouwensraamwerk voor datadeling tussen organisaties met
publieke taken, die deelnemer van het stelsel zijn. Het FDS bestaat uit:

- **Functies** Dit zijn de Stelselfuncties die zorgen voor de werking en doorontwikkeling van het FDS. We
onderscheiden organisatorische functies en technische functies.

- **Afspraken** Voorbeelden hiervan zijn de aansluitvoorwaarden voor aanbieders, de voorwaarden voor
gebruikers en het kwaliteitsraamwerk.

- **Standaarden** Bekende voorbeelden zijn DCAT2.0, voor het gestandaardiseerd beschrijven van data en
dataservices, en SKOS, een standaard voor het vastleggen van begrippen.

- **Voorzieningen** die het verantwoord datadelen ondersteunen. Hierbij geldt het principe: 'decentraal
wat kan, centraal wat moet'. Een voorbeeld van een centrale voorziening is de zogenaamde
Informatiekundige kern, die het mogelijk maakt om te navigeren en data te combineren in het FDS.

Hieronder gaan we in op de ontwikkeling van de Stelselfuncties. Doel: inzicht geven in de
prioriteiten en de ontwikkelaanpak.

<div class="stelselfunctie-blok-container container">
  <div class="stelselfunctie-blok-header row">
    <h3>Organisatorische stelselfuncties</h3>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="#regisseur" class="stelselfunctie-blok col">
        <img src="images/regisseur.png" alt="Regisseur"/>
        <h3>Regisseur</h3>
        <p>
        Stelselfunctie voor de coördinatie van beheer en (door)ontwikkeling van het stelsel.
        </p>
    </a>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="#poortwachter" class="stelselfunctie-blok col">
      <img src="images/poortwachter.png" alt="Poortwachter"/>
      <h3>Poortwachter</h3>
      <p>
      Stelselfunctie voor toetsing, autorisatie en registratie van koppelingsverzoeken. Toetst op conformiteit met de AVG en de stelsel aansluit- en gebruiks-voorwaarden en zorgt daarbij voor een transparant beslisproces.
      </p>
    </a>
    <a href="#marktmeester" class="stelselfunctie-blok col">
      <img src="images/marktmeester.png" alt="Marktmeester"/>
      <h3>Marktmeester</h3>
      <p>Stelselfunctie voor het verbinden van vraag en aanbod.</p>
      <p>Ondersteunt bij het inzichtelijk maken van het potentieel en het realiseren van nieuwe gebruikerswensen.</p>
    </a>
    <a href="#helpdesk" class="stelselfunctie-blok col">
      <img src="images/helpdesk.png" alt="Helpdesk"/>
      <h3>Helpdesk</h3>
      <p>Stelselfunctie voor eerstelijnsondersteuning van gebruikers en geregistreerden.</p>
      <p>Tevens meldpunt voor het melden van fouten in overheidsregistraties.</p>
    </a>
    <a href="#expertisecentrum" class="stelselfunctie-blok col">
      <img src="images/expertisecentrum.png" alt="Expertisecentrum"/>
      <h3>Expertisecentrum</h3>
      <p>Stelselfunctie voor het onderhouden van de stelselarchitectuur en het faciliteren van de communities waarin kennis en ervaring over het stelsel wordt gedeeld.</p>
      <p>Het kennisnetwerk.</p>
    </a>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="#toezichthouder" class="stelselfunctie-blok col">
      <img src="images/toezichthouder.png" alt="Toezichthouder"/>
      <h3>Toezichthouder</h3>
      <p>Stelselfunctie die toeziet op het nakomen van stelselafspraken en die klachten en bezwaren daarover afhandelt.</p>
    </a>
  </div>
</div>

&nbsp;

<div class="stelselfunctie-blok-container container">
  <div class="stelselfunctie-blok-header row">
    <h3>Technische stelselfuncties</h3>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="#verantwoordingsfunctie" class="stelselfunctie-blok col-md-3">
      <b>Verantwoordingsfunctie</b>
      <p>Inzicht bieden in gerealiseerde en geautoriseerde koppelingen</p>
    </a>
    <a href="#catalogusfunctie" class="stelselfunctie-blok col-md-3">
      <b>Catalogusfunctie</b>
      <p>Inzicht bieden in het aanbod van data en dataservices</p>
    </a>
    <a href="#monitoringsfunctie" class="stelselfunctie-blok col-md-3">
      <b>Monitoringsfunctie</b>
      <p>Inzicht bieden in datavolume, datakwaliteit en service level</p>
    </a>
    <a href="#notificatiefunctie" class="stelselfunctie-blok col-md-3">
      <b>Notificatiefunctie</b>
      <p>Inzicht bieden in gebeurtenissen</p>
    </a>
    <a href="#terugmeldfunctie" class="stelselfunctie-blok col-md-3">
      <b>Terugmeldfunctie</b>
      <p>Digitaal melden van bevindingen mbt de datakwaliteit</p>
    </a>
    <a href="#toegang" class="stelselfunctie-blok col-md-3">
      <b>Toegang</b>
      <p>Identificatie, authenticatie en logging</p>
    </a>
  </div>
</div>

## Organisatorische stelselfuncties

### Regisseur

Coördineert beheer en (door)ontwikkeling van het stelsel.

#### Status

Ontwikkelingen tijdens het programma Realisatie IBDS geven inzicht in de manier waarop de
Regisseur-functie structureel moet worden ingericht.

Zodra de tijd rijp is, wordt een inrichtingsvoorstel opgesteld en ter goedkeuring voorgelegd via de
Stelsel-governance.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Poortwachter

- Toetst, autoriseert en registreert koppelingsverzoeken tussen bronnen. Toetst op conformiteit met
  de AVG en de stelsel aansluit- en gebruiksvoorwaarden.
- Zorgt voor een transparant beslisproces.

#### Status

- Er is een startnotitie gemaakt.
- In de tweede helft van 2023 is de beschrijving van de werking verdiept langs een aantal dimensies:
  functies, standaarden, voorzieningen en wettelijk inbedding. Dit als opmaat naar demonstrators en
  experimenten in het Digilab om de feitelijke werking zo concreet mogelijk te maken.

#### Achtergrond

// TODO

#### FDS context & positionering

Kernelement van het federatief datastelsel is dat datagebruik in overeenstemming is met publieke
waarden. Om dit waar te maken, is het nodig dat ontwerp en realisatie van het FDS vanuit dit
perspectief worden gestuurd. Daartoe zijn de volgende ontwerpelementen passend:

- Poortwachtersfunctie.
- Dataminimalisatie.
- Regie op Gegevens.

Het stelsel voorziet in gecontroleerde toegang tot gevoelige data. Voor het toegang bieden en
afnemen van data die niet gevoelig zijn, volstaan veelal algemene toegangsregels, bijvoorbeeld bij
een bron die als open data is aangemerkt. Voor gevoelige stelseldata zoals persoonsgegevens, waarbij
gebruikt wordt gemaakt van het BSN als identificatie en koppelingssleutel, is meer nodig. Deze data
passeren eerst de stelselfunctie Poortwachter.  
De poortwachters is de functie binnen het Federatief Datastelsel die op basis van een formeel
vastgesteld toetsingskader bepaalt of de combinatie data - gebruikersgroep - gebruiksdoel kan worden
toegestaan. Dit is een drempel om ervoor te zorgen dat er een zorgvuldige afweging wordt gemaakt
tussen de vaak tegenstrijdige maatschappelijke belangen bij nieuwe vormen van sector overstijgend
datagebruik.

Door de verantwoordelijkheid voor deze toetsing op stelselniveau te beleggen, kan kennis over
verantwoord datagebruik worden gebundeld en ontstaan er meer mogelijkheden om de afweging
transparant en controleerbaar te maken. Dit maakt het proces efficiënter doordat individuele
data-aanbieders worden ontzorgd omdat ze op uitvoeringsniveau geen belangenafweging meer hoeven te
maken die eigenlijk op het politieke niveau thuishoort.

Onderdeel van het toetsingskader is het benutten van de verschillende opties die het stelsel biedt
om verantwoord data te delen. Zo ondersteunt het toekomstig stelsel dataminimalisatie waarbij de
verstrekkers van stelseldata _privacy enhancing technologies_ toepassen of intelligente
bevragingsdiensten leveren die gericht antwoord geven op een gestelde informatievraag (bijvoorbeeld
is deze persoon jonger dan 18?).

In een aantal gevallen is het dan niet meer nodig om (privacy)gevoelige data te delen. Een andere
mogelijkheid die het stelsel ondersteunt is het faciliteren dat de burger of het bedrijf zelf
toestemming geeft om persoonlijke of bedrijfsdata via het federatief datastelsel met anderen te
delen.

Zie ook [Position paper Policy Based Access Control](../../bouwblokken/pbac/).

#### Inhoudelijke verdieping

De Poortwachtersfunctie zorgt voor toetsing, autorisatie en registratie van verzoeken om data te
delen1 . De toetsing zal veelal neerkomen op het afwegen van strijdige belangen, waarbij de volgende
zaken een rol spelen:  

- Noodzaak (er zijn geen alternatieven, digitaal werken is nodig, het gaat om tot de persoon
  herleidbare of concurrentiegevoelige gegevens).  
- Het maatschappelijke belang  
- Het recht op bescherming van de persoonlijke levenssfeer (bescherming privacy).
- Juridische toelaatbaarheid.
- Ethische toelaatbaarheid.
- Maatschappelijke wenselijkheid.
- Conformiteit met aansluit- en gebruiksvoorwaarden van het federatief datastelsel.

Dit gebeurt in een transparant beslisproces met mogelijkheden voor bezwaar en beroep. De uitkomst
van dit proces wordt vastgelegd in een openbare registratie (gegevensstroom boekhouding).  Deze
registratie heeft de volgende functies:  

- Opname van een datadeelrelatie in het register legitimeert de datadeling door daarvoor een
  juridische grondslag te bieden. Deze grondslag is een verwijzing naar een toepasselijk wetsartikel
  of een positief besluit van het toetsingscomité.  
- Het maakt op één plek inzichtelijk welke stelseldata, met welke partijen, voor welk doel mogen
  worden gedeeld en wat daarvoor de juridische grondslag is. Hiermee kan ook periodiek worden
  getoetst of eerder genomen beslissingen nog steeds valide zijn. Daarnaast ontstaat inzicht op
  welke gebieden aanpassing van wetgeving nodig is om oordelen van het toetsingscomité juridisch
  sterker te verankeren.  
- Op het federatief datastelsel aangesloten data aanbieders kunnen op basis van de gegevens in het
  register eenvoudig, geautomatiseerd, vaststellen of ze een datadeel verzoek van een tot het
  stelsel toegelaten afnemer mogen honoreren. Ze hoeven dus niet meer zelf een juridische toets uit
  te voeren.  
- Vastlegging in het centrale register van datadeel relaties, in combinatie met de toelating tot het
  stelsel (het voldoen aan de aansluitvoorwaarden) vervangt de bilaterale contracten die nu nog vaak
  nodig zijn om de datadeling tussen partijen te legitimeren.  

De Poortwachtersfunctie is één van de structurele stelselfuncties die het project Federatief
Datastelsel de komende jaren zal realiseren. Bij de realisatie wordt gebruik gemaakt van de
resultaten van andere onderdelen van het programma Realisatie Interbestuurlijke Datastrategie
(Adviesfunctie verantwoord datagebruik, het organiseren van datadialogen) en de uitkomsten van
relevante externe trajecten zoals het wetgevingswerk van de Regeringscommissaris
Informatiehuishouding.

De eerste stap is het ontwerp van deze functie, vergelijkbaar met de wijze waarop de inrichting van
de [Marktmeesterfunctie](#marktmeester) is beschreven. Daarna zal de feitelijke inrichting via use
cases / praktijksituaties plaatsvinden. Het tempo hangt daarmee af van de snelheid waarmee geschikte
use cases zich voordoen. Parallel wordt de technische functionaliteit van de Poortwachtersfunctie
beproefd (m.b.v. de Integrale Gebruiksoplossing, de IGO) in de Innovatiewerkplaats.

#### Standaardisatie

// TODO

### Marktmeester

- Verbindt vraag en aanbod van data voor sectoroverstijgend, meervoudig gebruik.
- Helpt het gebruikspotentieel van het Federatief Datastelsel inzichtelijk te maken en ondersteunt
  stelselgebruikers bij het inbrengen van hun eisen en wensen omtrent data voor maatschappelijke
  opgaven.

#### Status

- Er is een inrichtingsvoorstel gemaakt voor de invulling van de Marktmeester functie tijdens de
  projectfase waarbij er een centraal aanspreekpunt is. Dit voorstel is besproken in het [Tactisch
  Stelseloverleg van 30 mei
  2023](https://realisatieibds.pleio.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/federatief-datastelsel/wiki/view/27e82cd2-669b-45e2-8a29-59d192141128/agenda-notulen-tactisch-overleg-30-mei).

- Met deze tijdelijke invulling wordt de komende tijd ervaring opgedaan in samenwerking met het
  project Data in maatschappelijke opgaven. Deze praktijkervaring wordt gebruikt om later een
  voorstel voor de definitieve inrichting van deze stelselfunctie te doen.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Helpdesk

Eerstelijnsondersteuning van gebruikers en geregistreerden.

#### Status

Deze functie wordt samen met het kenniscentrum van de Interbestuurlijke Datastrategie (IBDS)
vormgegeven en groeit mee met de behoeften van aanbieders en gebruikers.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Expertisecentrum

- Onderhoudt stelsel-architectuur.
- Faciliteert community's waarin kennis en ervaring over het stelsel wordt gedeeld.
- Kennisnetwerk.

#### Status

- Ontwikkelingen tijdens het programma Realisatie IBDS geven inzicht in de manier waarop het
  Expertisecentrum structureel moet worden ingericht.
- Zodra de tijd rijp is, wordt een inrichtingsvoorstel opgesteld en ter goedkeuring voorgelegd via
  de Stelsel-governance.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Toezichthouder

- Ziet toe op de nakoming van stelselafspraken.
- Handelt klachten en bezwaren af.

#### Status

Deze functie bestaat al in het huidige Stelsel van Basisregistraties. Deze rol wordt over het
algemeen ingevuld door de verantwoordelijk opdrachtgever voor de registratie. Dit wordt op termijn
en in samenspraak met omgevormd tot een passende toezichtfunctie voor het FDS.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

## Technische stelselfuncties

### Verantwoordingsfunctie

- **Doel** Inzicht bieden in gerealiseerde en geautoriseerde koppelingen.
- **Status** Dit is een technische subfunctie van de Poortwachtersfunctie en wordt binnen dat kader
  (en via beproeving in het Digilab) ontwikkeld.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Catalogusfunctie

- **Doel** Inzicht bieden in het aanbod van data en dataservices.
- **Status** Er is een federatieve catalogus voorzien, om inzicht en overzicht te bieden in
  beschikbare data en dataservices.

#### Achtergrond

De in- en overzichtfunctie is één van de stelselfuncties van het te realiseren Federatief
Datastelsel (FDS). Deze stelselfunctie maakt het mogelijk om vanuit verschillende perspectieven een
totaalbeeld te krijgen van de inhoud van het FDS: welke data conform het FDS-afsprakenkader wordt
gedeeld, waarvoor deze data kan worden gebruikt, welke partijen betrokken zijn bij dit gebruik en
hoe deze zich binnen de federatie tot elkaar verhouden.

#### FDS context & positionering

Dit wordt mogelijk omdat is afgesproken dat alle componenten van het FDS (deelnemers, datasets en
datadiensten) van betekenisvolle beschrijvende gegevens (metadata) worden voorzien die op een
gestandaardiseerde manier openbaar beschikbaar worden gesteld. Hiermee ontstaat een virtuele open
(meta)dataset, een ‘catalogus’, die op verschillende manieren kan worden ontsloten en doorzocht.

Zo kan bijvoorbeeld worden opgezocht welke gegevens er in de basisregistratie WOZ zijn opgenomen,
wat de betekenis en de kwaliteit daarvan is en met welke datadiensten dit aanbod is ontsloten.
Hoewel de in- en overzicht functie op zich alleen zaken passief toont (overzicht biedt), levert dit
wel het inzicht dat nodig is om op een verantwoorde manier actief met het datapotentieel aan de slag
te kunnen gaan.  

Omdat de gekozen afspraken en standaarden het mogelijk maken om ‘alles met alles’ te verbinden en de
FDS-metadata zelf ook een volgens de FDS-standaarden ontsloten (virtuele) dataset is, kan iedereen
naar eigen behoefte met de open FDS-metadata aan de slag. Zo is het bijvoorbeeld mogelijk om een
catalogus te bouwen waarin wetgevingsjuristen eenvoudig kunnen zoeken of een bepaald begrip al in
bestaande FDS-data voorkomt, welke definitie het daar heeft en aan welke juridische grondslag (wet,
regeling) deze definitie is ontleend. Gebruikers die bijvoorbeeld met het thema energietransitie aan
de slag willen, kunnen op basis van daarmee verbonden trefwoorden zoeken welke data daarvoor in het
FDS beschikbaar is en of deze voor hen bruikbaar zijn (qua toegang, kwaliteit, betekenis etc.) Dat
zoeken kan in de eigen catalogus van het FDS, maar het is ook mogelijk om de FDS-metadatadiensten in
een eigen energiecatalogus te integreren die ook niet-FDS-data ontsluit om zo de gebruikers een
totaalbeeld van het energiedata-aanbod te geven. Omdat de FDS afspraken en standaarden voor het
ontsluiten van meta data het mogelijk maken om op begrippen te zoeken, ondersteunen ze ook het
oppakken van semantische harmonisatie en andere acties om verschillende databronnen beter op elkaar
te laten aansluiten en om wetgeving en data beter met elkaar te verbinden. Ze maken het namelijk
mogelijk om een overzicht te krijgen van begrippen met een verschillende naam, maar dezelfde
betekenis (synoniemen), van begrippen met dezelfde naam maar juist een verschillende betekenis
(homoniemen), of van begrippen die zijn opgebouwd uit een combinatie van een aantal andere begrippen
(concepten).  
De in- en overzichtfunctie is daarmee een belangrijk instrument voor het creëren van waarde met de
in het FDS opgenomen data.  

De in- en overzichtfunctie is ook vanuit een ander perspectief van belang. Omdat iedereen de
metadata kan raadplegen (het is immers open data), wordt de inhoud van het FDS volledig transparant,
waardoor dit ook onderwerp kan zijn van publiek debat. Dit is een bewuste keuze omdat bij het FDS de
publieke waarden centraal staan. Deze openheid betreft uiteraard alleen de beschrijvende data (de
metadata) en niet de data zelf. De individuele gegevens binnen de datasets zijn en blijven alleen
toegankelijk voor degenen die daartoe zijn geautoriseerd.  

##### Beoogde werking

Voor het aanbieden van de metadata gelden dezelfde principes als voor andere FDS-data. Dit betekent
dat degene die data in het stelsel aanbiedt zelf de kenmerken van deze data beschrijft en zelf
diensten levert om deze metadata ‘bij de bron’ te kunnen bevragen. Door aan te sluiten bij gangbare
wereldwijde, Europese en nationale standaarden voor het generen, vastleggen en ontsluiten van
metadata wordt deze meervoudig bruikbaar. Data-aanbieders hoeven dit in principe slechts één keer te
doen om hun aanbod in alle voor hen relevante datastelsels (dataspaces) voor mensen en machines
vindbaar te maken. Het op het FDS toegesneden data-aanbod kan hieruit gedestilleerd worden door in
de metadata een ‘FDS-label’ op te nemen als in het aansluitproces is vastgesteld dat aan de
FDS-kwaliteitseisen wordt voldaan. Door voor dit labelen een stelseloverstijgende standaard af te
spreken, kunnen data-aanbieders op uniforme wijze in de metadata laten zien aan welke
afsprakenstelsels ze voldoen, dus in welke datastelsels hun data gebruikt kunnen worden. Met deze
aanpak wordt de variëteit in het datagebruik ondersteund en worden tevens groeipaden gefaciliteerd.
Open overheidsdata waarvan de metadata al wel op orde is, maar die bijvoorbeeld alleen nog maar als
download beschikbaar is, kan al meteen op data.overheid.nl worden aangeboden terwijl deze pas later,
nadat bijvoorbeeld door het toevoegen van een API het FDS-label is verdiend, ook in de FDS-catalogus
verschijnt.  

###### Van visie naar werkelijkheid: het realisatietraject

In de kop van de vorige paragrafen werd het woord “beoogd” gebruikt. Dat illustreert dat er nog het
nodige te doen valt voordat we zover zijn. Soms betreft dit ontwikkeling omdat de benodigde metadata
standaarden er nog niet zijn, soms betreft dit het kiezen uit verschillende concurrerende
standaarden en soms is de standaard duidelijk maar moet deze nog op de Nederlandse situatie worden
toegespitst of moet er worden afgesproken hoe je de standaard precies gaat gebruiken.

Zelfs als dit alles achter de rug is, moet er nog ervaring worden opgedaan met het in onderlinge
samenhang toepassen van deze standaarden om het verlangde inzicht en overzicht op FDS-(totaal)niveau
te krijgen. Als elke FDS-deelnemer zelf het wiel uit moet vinden, zijn we vele jaren verder voordat
deze essentiële stelselfunctie voldoende bruikbaar is. Het FDS-realisatieproject zal daarom
ondersteuning bieden om de realisatie en implementatie te versnellen. Daarbij wordt de volgende
aanpak gehanteerd:  

- Vertrekpunt is het denkwerk dat al binnen de Nora-community is gedaan (zie de bijlage onderaan dit
  hoofdstuk).  

- Het FDS-project onderneemt in samenwerking met de verantwoordelijke beheerders van bestaande
  metadata voorzieningen en de (potentiële) data-aanbieders actie om de hierna genoemde
  voorzieningen verder te ontwikkelen. Dit als tijdelijke overbrugging naar de uiteindelijke
  oplossing van een op afspraken en standaarden gebaseerd federatief werkend virtueel geheel, met
  voor het FDS mogelijk nog een eigen ‘catalogus’ ingang om eenvoudig een overzicht te krijgen van
  het data aanbod dat aan de eisen van het FDS-afsprakenstelsel voldoet.

- Voor het beschrijven van datasets is dit nu data.overheid.nl. De relevante standaard hiervoor is
  DCAT.  

- Voor het beschrijven van de in data opgenomen gegevens is dit nu de stelselcatalogus. Relevante
  standaarden hiervoor zijn SKOS, MIM en de standaard voor het beschrijven van begrippen (SBB).

- Voor het beschrijven van dataservices (API’s) is dit developer.overheid.nl.  

- Voor nieuwe elementen zoals het beschrijven van de stelseldeelnemers en de kwaliteit van data en
  -diensten (het inhoud geven aan het FDS-label) ontwikkelt het FDS-project in samenwerking met
  kennisinstituten zoals Geonovum nieuwe standaarden die in het Digilab worden beproefd om te
  verifiëren of ze praktisch werkbaar zijn.  

- Het FDS zorgt voor het ontwikkelen van afspraken en standaarden voor het betekenisvol verbinden
  van de verschillende metadatabronnen, waarbij met in het Digilab te ontwikkelen
  referentietoepassingen beproefd wordt of deze op een werkbare manier het beoogde totaalbeeld
  opleveren. Hierbij wordt aansluiting gezocht bij proeven en pilots die voor de EU of sectorale
  dataspaces worden gedaan.  

- Als is aangetoond dat het werkt en werkbaar is, worden koplopers gezocht die de componenten van de
  FDS in- en overzichtfunctie voor (een deel van) hun aanbod willen implementeren. Hiermee wordt
  duidelijk wat er nodig is om het samenhangend geheel van bestaande en nieuwe meta data standaarden
  in de uitvoeringspraktijk toe te passen en komen er ervaringscijfers beschikbaar waarmee geraamd
  kan worden hoeveel tijd en geld er nodig is om ze stelselbreed toe te gaan passen.  

- De opgedane praktijkervaring wordt vervolgens benut om de formele besluitvorming over de
  opschaling in gang te zetten, met behandeling van de voorstellen in het Interbestuurlijk Data
  Overleg (IDO) en het Overheidsbreed Beleidsoverleg Digitale Overheid (OBDO) en een openbare
  consultatie van volgens de bestaande procedures van het Forum Standaardisatie.  

- Als de formele besluitvorming succesvol is afgerond zal het FDS-project de adoptie aanjagen door
  de stelselpartijen te ondersteunen bij de implementatie van de (nieuwe) metadata-afspraken en
  -standaarden. Hierbij kan gedacht worden aan het beschikbaar stellen van validators, het
  faciliteren van proefimplementaties en het delen van voorbeelden en best practices.

#### Standaardisatie

In de NORA wordt gewerkt aan het beter in samenhang duiden van onderwerpen die nu afzonderlijk zijn
beschreven. Daartoe is in de NORA gebruikersraad van september 2023 een opzet besproken van
onderwerpen die raken aan het thema Semantiek en Gegevensmanagement.  

### Monitoringsfunctie

- **Doel** Inzicht bieden in datavolume, datakwaliteit en service level.
- **Status** Deze wordt relevant tegen de tijd dat het huidige stelsel is uitgebreid met nieuwe
  stelseldata. Deze functie wordt in het huidige stelsel per registratie anders ingericht. Bij
  uitbreiding zal hier meer uniformiteit in worden gebracht.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Notificatiefunctie

- **Doel** Inzicht bieden in gebeurtenissen
- **Status** Dit is een zeer belangrijke functie om [Data bij de
  bron](/docs/realisatie/toekomstbeeld/data-bij-de-bron/) te laten slagen. Want als vitale data
  elders wordt beheerd, verliezen gebruikers het zicht op relevante mutaties in relatie tot hun
  eigen proces. De notificatiefunctie zorgt ervoor dat gebruikers zicht en grip houden op mutaties
  van relevante data bij een processtap.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Terugmeldfunctie

- **Doel** Digitaal melden van bevindingen met betrekking tot de datakwaliteit
- **Status** Een essentiële functie voor de kwaliteit van stelseldata: gebruikers hebben het beste
  zicht op de juistheid van (hun) gegevens en moeten de mogelijkheid hebben om fouten en gerede
  twijfel te melden. Uit de pilot Makkelijk Melden zijn waardevolle inzichten voortgekomen over deze
  functie.

#### Achtergrond

// TODO

#### FDS context & positionering

// TODO

#### Inhoudelijke verdieping

// TODO

#### Standaardisatie

// TODO

### Toegang

- **Doel** Identificatie, authenticatie en logging. Het gaat om verantwoording datagebruik in
  relatie tot de wettelijke grondslagen.
- **Status** Dit is een technische subfunctie van de Poortwachtersfunctie en wordt binnen dat kader
  (en via beproeving in het Digilab) ontwikkeld.

#### Achtergrond

Her en der zijn er diverse definities in gebruik voor de begrippen identificatie, authenticatie en
autorisatie. Voor het federatief datastelsel is ooit de technische functie **identificatie,
authenticatie en logging (beveiligings- en veranwoordingsfunctie)** benoemd.

#### FDS context & positionering

Voor het federatief datastelsel gaat het als [stelselfunctie](/kennisbank/stelselfuncties/) om
toegang tot data onder de juiste voorwaarden, de API van de service die data deelt met een
bevragende toepassing, client. Het federatief datastelsel voorziet ook op dit vlak in afspraken en
standaarden.

De voorwaarden die van toepassing zijn bestaan uit het FDS afsprakenkader dat eventueel is aangevuld
met (sector) specifieke voorwaarden. Hierin zijn voorwaarden beschreven bij het gebruiksdoel en de
technische voorwaarden. Met de [in- en overzicht stelselfunctie] (_welke functie is dit precies?_)
weten gebruikers wat deze voorwaarden zijn.  

Aansluitend op de toegang zijn er technische functies voor monitoring en logging voorzien passend
bij de organisatorische functies van [poortwachter](#poortwachter) en
[toezichthouder](#toezichthouder) in het federatieve stelsel. Toegang onder de juiste voorwaarden
qua beveiliging en verantwoording is hiermee mogelijk.

Zie ook [Position paper Policy Based Access Control](../../bouwblokken/pbac/).

#### Inhoudelijke verdieping

##### Identificatie en authenticatie

Voor het delen van data is identificatie van de aanbieder en afnemer nodig. Het digitale
identiteitenbeheer van personen en organisaties (natuurlijke en niet-natuurlijke personen) is een
vraagstuk op zich.

Voor het FDS gaan we er vanuit dat een partij beschikt over een middel dat een digitale identiteit
aantoont. De authenticatie betreft de verificatie hiervan. Vervolgens is het middel in te zetten om
te komen tot het wederzijds veilig opzetten van een verbinding voor de uitwisseling van data.

##### Delegatie

Het kan zijn dat een bevragende toepassing werkt met een gedelegeeerde toegangsfunctie. In een
dergelijk geval bevraagt de toepassing namens een bepaalde partij anders dan eigenaar een service.
Die partij verleent de toepassing de bevoegdheid voor de bevraging.

##### Autorisatie met verantwoording

Kernelement van het federatief datastelsel is dat datagebruik in overeenstemming is met publieke
waarden. Het stelsel voorziet in gecontroleerde toegang tot gevoelige data. Voor het toegang bieden
en afnemen van data die niet gevoelig zijn, volstaan veelal algemene toegangsregels, bijvoorbeeld
bij een aanbieder van open data.

Het bijpassende beleid dat bij een aanbieder en afnemer het gebruik bepaalt, de rechten van gebruik
toekent vormt de autorisatie. Dit is direct verbonden met de voorwaarden die van toepassing zijn
voor de betreffende data.

Logging die hier zo mogelijk een rol bij speelt met oog op verantwoording is bij zowel afnemer als
aanbieder aanwezig (afhankelijk van de voorwaarden, gevoeligheid van data).

Aan de kant van de afnemer ligt de verantwoordelijkheid om goed om te gaan met de data, bijvoorbeeld
om er voor te zorgen dat een eindgebruiker van een toepassing binnen een aangegeven doelbinding
blijft.

#### Standaardisatie

Een standaard waarmee in het FDS toegang langs deze route is in te regelen is de [Federatieve
Service Connectiviteit Standaard](/kennisbank/fsc/).
