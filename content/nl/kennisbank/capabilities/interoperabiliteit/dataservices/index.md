---
title: Dataservices
weight: 2
date: 2023-10-17
categories: [Capabilities, Interoperabiliteit, Dataservices]
tags: 
description: >
  Interoperabiliteit | Dataservices: API's voor data uitwisseling
---

{{< capabilities-diagram selected="dataservices" >}}

## Beschrijving

Deze capability vergemakkelijkt het delen en uitwisselen van gegevens (d.w.z. gegevensvoorziening en
dataconsumptie/gebruik) tussen datastelsel/dataspace-deelnemers. Een voorbeeld van een bouwblok voor
data-interoperabiliteit die een gemeenschappelijke gegevensuitwisseling API biedt, is de
‘contextmakelaar’ van de Connecting Europe Facility (CEF), die door de Europese Commissie wordt
aanbevolen voor het delen van juiste gegevens over meerdere organisaties.

## Bouwblokken

- [Dataservice](/kennisbank/uitwisseling-apis/)
- [Self-description](/kennisbank/self-description/)
