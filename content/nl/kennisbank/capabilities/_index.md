---
title: Capabilities
# menu: {main: {weight: 20}}
weight: 10
description: >
   De capabilities (bekwaamheden) van het federatief datastelsel met relaties (verwijzingen)
   naar de **bouwblokken** die voor de realisatie nodig zijn
status: proposed
date: 2023-11-28
author: R-FDS Team
version: v0.1
---

Het federatief datastelsel gebruikt een [Raamwerk voor ontwikkeling in
samenwerking](/kennisbank/raamwerk/). Hieronder is de uitwerking van de capabilities die daar
benoemd worden.

Klik op de betreffende capability om te lezen waar deze over gaat, welke componenten,
afhankelijkheden en relaties deze heeft.

{{< capabilities-diagram >}}

&nbsp;  

Voor meer informatie over [Capabilities](/besluiten/00001-basisstructuur/#capabilities) en de
context van het federatief datastelsel, lees onze [Strategie van
samenwerken](/docs/strategie-van-samenwerken/) en het [Raamwerk voor ontwikkeling in
samenwerking](/kennisbank/raamwerk/). In de [Werkomgeving](/docs/werkomgeving/) kun je lezen hoe je
kunt bijdragen en hoe de samenwerkomgeving is opgezet. Wil je meer lezen over achterliggende
overwegingen (besluiten), lees onze DR [00001 Basisstructuur](/besluiten/00001-basisstructuur/) en
DR [0004 Capabilities NL](/besluiten/00004-capabilities-nl/).
