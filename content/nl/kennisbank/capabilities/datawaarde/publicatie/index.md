---
title: Publicatie
weight: 3
date: 2023-10-17
categories: [Capabilities, Datawaarde, Publicatie]
tags: 
description: >
  Datawaarde | Publicatie: Publicatie- en marktplaatsdiensten
---

{{< capabilities-diagram selected="publicatie" >}}

## Beschrijving

Om het aanbod van databronnen en -diensten onder gedefinieerde voorwaarden te ondersteunen, moeten
marktplaatsen worden opgericht. Deze capability ondersteunt de publicatie van dit aanbod, het beheer
van processen die verband houden met het creëren en monitoren van slimme contracten (die duidelijk
de rechten en plichten voor data- en servicegebruik beschrijven), en toegang tot data en diensten.

## Bouwblokken

- [FSC](/kennisbank/fsc/)
- [...]
