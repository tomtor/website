---
title: Identiteit
weight: 1
date: 2023-10-17
categories: [Capabilities, Vertrouwen, Identiteit]
tags: 
description: >
  Vertrouwen | Identiteit: Identiteitsbeheer
---

{{< capabilities-diagram selected="identiteit" >}}

## Beschrijving

Deze capability maakt identificatie, authenticatie en autorisatie mogelijk van belanghebbenden die
in het federatief datastelsel of een dataspace actief zijn. Het zorgt ervoor dat organisaties,
individuen, machines en andere actoren worden voorzien van erkende identiteiten en dat die
identiteiten kunnen worden geauthenticeerd en geverifieerd, inclusief aanvullende
informatievoorziening. Deze erkende identiteiten kunnen worden gebruikt door autorisatiemechanismen
om toegang en gebruikscontrole mogelijk te maken.

Identiteitsbeheer kan worden geïmplementeerd op basis van direct beschikbare Identity
Management-platformen die delen van de vereiste functionaliteit behandelen. Voorbeelden van
open-source oplossingen zijn de Keycloak-infrastructuur, het Apache Syncope IM-platform, het
open-source IM-platform van het Shibboleth Consortium of het Fiware IM-framework. Integratie van de
capability identiteit met de eID-bouwsteen van de Connecting Europe Facility (CEF), ter
ondersteuning van elektronische identificatie van gebruikers in heel Europa, zou met name belangrijk
zijn. Creatie van federale en vertrouwde identiteiten in dataspaces kan worden ondersteund door
Europese voorschriften zoals EIDAS.

## Bouwblokken

- [FSC](/kennisbank/fsc/)
- [...]
