---
title: Toegang
weight: 2
date: 2023-10-17
categories: [Capabilities, Vertrouwen, Toegang]
tags: 
description: >
  Vertrouwen | Toegang: Toegangscontrole en gebruikscontrole
---

{{< capabilities-diagram selected="toegang" >}}

## Beschrijving

Deze capability garandeert de handhaving van het beleid inzake datatoegang en -gebruik gedefinieerd
als onderdeel van de algemene voorwaarden die zijn vastgesteld wanneer databronnen of -services
worden gepubliceerd (zie capability
[Datawaarde | Publicatie](/kennisbank/capabilities/datawaarde/publicatie/)) of onderhandeld tussen providers
en consumenten. Een data-aanbieder implementeert doorgaans mechanismen voor datatoegang om misbruik
van middelen te voorkomen, terwijl het besturingsmechanismen voor datagebruik meestal aan de kant
van de data-afnemer worden geïmplementeerd om misbruik van data te voorkomen. In complexe
datawaardeketens worden beide mechanismen gecombineerd door 'prosumeurs'. Toegangscontrole en
gebruikscontrole vertrouwen op identificatie en authenticatie (zie capability
[Vertrouwen | Identiteit](/kennisbank/capabilities/vertrouwen/identiteit/)).

## Bouwblokken

- [Position paper Policy Based Access Control](../../../bouwblokken/pbac/)
- [FSC](/kennisbank/fsc/)
- [...]
