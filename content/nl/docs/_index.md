---
title: Welkom
# linkTitle: Docs
menu: {main: {weight: 20}}
weight: 20
---

Welkom bij federatief.datastelsel.nl, het  kennisplatform voor ons allemaal!

Het federatief datastelsel is een open source project dat voor iedereen toegankelijk is om te
gebruiken en te verbeteren. De ontwikkeling hiervan doen we juist graag met elkaar, dus we kijken
uit naar je betrokkenheid.

We leggen je graag uit hoe we met elkaar willen samenwerken. In [strategie van
samenwerken](/docs/strategie-van-samenwerken/) staat hoe we de samenwerking voor ons zien.
Bij [werkomgeving](./werkomgeving/) hopen we jullie inzicht en duidelijkheid te geven in
hoe we  die samenwerking dan het liefste willen realiseren.

Heb je vragen? Gaan dan direct naar [MatterMost](https://digilab.overheid.nl/chat/fds) (onze chat)!
Bij het kanaal [~algemeen](https://digilab.overheid.nl/chat/fds/channels/town-square) in MatterMost
kun je al je vragen stellen. Mocht je dan willen aanhaken bij een specifiek thema dan zorgen we dat
je geattendeerd wordt op het juiste kanaal!
