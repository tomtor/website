---
title: 00003 Werkomgeving voor ontwikkeling
type: besluiten
date: 2023-10-25
author: R-FDS Team
categories: [DR]
tags: 
status: draft
description: >
   Besluit voor de inrichting van de werkomgeving voor ontwikkeling
   gebaseerd op Git, Markdown, produceren van de website en meer
---

## Context en probleemstelling

Gegeven de context van de [Realisatie van de Interbestuurlijke
Datastrategie](https://realisatieibds.pleio.nl/) en de
[aanpak](https://realisatieibds.pleio.nl/cms/view/f17bdd08-7441-4968-95d4-acafdce49efb/over-de-ibds)
daarvan, dient het programma invulling te geven aan het aanjagen van de ontwikkelingen, verbinding
en samenwerking tot stand te brengen en te bevorderen en partijen over ketens heen bij elkaar te
brengen. Dat is nogal een ambitie.

Wat is hiervoor nodig? Wat is hier een goede invulling van? Welke voorzieningen dienen er te zijn om
dit allemaal tot stand te brengen?

## Beslissingsfactoren <!-- optional -->

- Wettelijk kader:
  - [...]
- Context:
  - Doelstellingen [Regie op
    Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)
  - Doelstellingen [Realisatie Interbestuurlijke Datastrategie
    (IBDS)](https://realisatieibds.pleio.nl/)
  - Doelstellingen [Federatief Datastelsel
    (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
  - Doelstellingen [Common Ground](https://commonground.nl/)
  - Doelstellingen [Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)
  - Doelstellingen [Digilab](https://digilab.overheid.nl/)
- Decision Records:
  - DR [00001 Basisstructuur](../00001-basisstructuur/)
- OpenDEI:
  - Governance
    - Overkoepelende samenwerkingsovereenkomsten

## Overwogen opties

- [Microsoft 365](#microsoft-365)
- [Google Workspace](#google-workspace)
- [The Good Cloud](#the-good-cloud)
- [Pleio](#pleio)
- [Markdown met Git](#markdown-met-git)
- [GitHub](#github)
- [GitLab](#gitlab)
- [NORA Wiki](#nora-wiki)
- [Static website](#static-website)

## Besluit

In het federatief datastelsel faciliteren wij samenwerking en besluitvorming. Dat is gebaat bij of
is zelfs alleen mogelijk door open te werken.

- **Wij werken open**
- **Wij werken community gedreven**
- **Wij verbinden**
- **Wij nemen faciliterend initiatief**

Om al deze stellingen goed te kunnen ondersteunen, is tooling nodig om samenwerking goed mogelijk te
maken. Dat is vooral tooling ter ondersteuning van open communities. Daarom kijken we hiervoor naar
de ervaringen van open source werken. Een open source community onderkent vier communicatie vormen /
kanalen:

1. Publicatie -> de landingsplaats voor iedereen en formele(re) communicatie van binnen naar buiten
   en met elkaar
2. Interactie -> Chat & Online video conferencing; korte, vluchtige en toegankelijke communicatie
3. Documenten -> onderwerpen waar samengewerkt wordt in documenten, waar bijdragen voor iedereen
   geleverd moeten kunnen worden
4. Issue tracking -> vragen, opmerkingen, suggesties, inhoudelijke discussies en vooral de opvolging
   daarvan

Wij kiezen graag voor _**best of breed**_ tools, welke dan wél zoveel mogelijk _geïntegreerd_ werken
en toegankelijk zijn. Aangezien wij community gedreven zijn en faciliterend initiatief nemen, maken
wij de werkomgeving beter, werkend en tot een prettige werkervaring voor zoveel mogelijk mensen.
Vanaf het begin zal dat nog niet helemaal zo beschikbaar zijn, maar daar werken we wel actief aan!

![Community tools](images/community-tools.jpg)

Voor **Publicatie** gebruiken we **Pleio** én een eigen **website**:
- [realisatieibds.pleio.nl](https://realisatieibds.pleio.nl/) is de formele publicatie en communicatie kanaal.
- [federatief.datastelsel.nl](https://federatief.datastelsel.nl) is de 'static' website

Voor de publicatie is het onhandig om _twee_ locaties aan te bieden. _Dit is een open punt dat nog
opgelost moet worden_.

Voor **Documenten** gebruiken we **Markdown met Git** en deze beheren we in **GitLab**. Voor de
content van deze website kiezen we voor
[gitlab.com/datastelsel.nl/federatief/website](https://gitlab.com/datastelsel.nl/federatief/website/).
Er zullen meer (git) repositories bijkomen en dit is nog open voor contributies.

[nog onder discussie] Voor **Chat** gebruiken we **Mattermost**:
[digilab.overheid.nl/chat/fds](https://digilab.overheid.nl/chat/fds/). Intern is MS Teams (nog?)
veelgebruikt.

Voor **Issue tracking** hebben we (nog) geen oplossing gekozen.

### Positieve gevolgen <!-- optional -->

- … <!-- numbers of options can vary -->

### Negatieve Consequences <!-- optional -->

- … <!-- numbers of options can vary -->

## Pros en Cons van de oplossingen <!-- optional -->

### Microsoft 365

> // TODO ...

Cons:
- Gesloten omgeving
- Gesloten en betaalde tools (Microsoft Office Suite)
- Amerikaanse cloud omgeving
- Microsoft betrokkenheid

### Google Workspace

> // TODO ...

Cons:
- Gesloten omgeving
- Gesloten en betaalde tools (Google Docs)
- Amerikaanse cloud omgeving
- Google betrokkenheid

### The Good Cloud

> // TODO ...

Pros:
- Europese cloud omgeving

Cons:
- Gesloten omgeving
- Gesloten en betaalde tools (Libre Office?)

### Pleio

> // TODO ...

Pros:
- Nederlandse cloud omgeving

Cons:

### Markdown met Git

Bij de ontwikkeling van content, het zij op internet, het zij in documenten, is er behoefte aan
opmaak. Opmaak in Microsoft Word is specifiek en direct afhankelijk van de tool MS Word. Op internet
is de afhankelijkheid naar een tool er niet. Hoogstens van de HTML (+ CSS + JavaScript) standaarden
voor visualisatie mogelijkheden.

Om voor de ontwikkeling van content op internet niet afhankelijk te zijn van technische experts die
HTML+ kunnen schrijven, wordt er al lang gezocht naar manieren om met 'pseudo codes' opmaak aan te
geven in platte tekst. Wikitekst, zoals in de [NORA Wiki](#nora-wiki), is daar een voorbeeld van.
Het vroegere Wordperfect had daar ook mogelijkheden voor. Voor software documentatie is Markdown de
de facto standaard.

Markdown is een vrij eenvoudige specificatie van speciale codes die opmaak betekenis hebben. Door
een conversiestap van bron (source) document naar HTML kan een mooie HTML website gepubliceerd
worden. Doordat Markdown redelijk gestandaardiseerd is, zijn er vele (😳) tools beschikbaar die een
website kunnen genereren. Hier is enorme keuze om een geschikte tool te vinden afhankelijk van de
situatie.

Doordat Markdown bestanden platte tekst bestanden zijn, is versiebeheer mbv Git makkelijk, logisch
en veel toegepast. Daarom is dit ook een prettige oplossing voor software developers documentatie.
Tools als [GitHub](#github) en [GitLab](#gitlab) hebben standaard ook ondersteuning voor Markdown
incl. presentatie als HTML.

- Goed, omdat Markdown eenvoudige codes gebruikt, zodat ook minder-technische mensen er mee uit de
  voeten kunnen (zie onze [Docs/Markdown](/docs/markdown/))
- Goed, omdat Markdown redelijk gestandaardiseerd is
- Goed, omdat versiebeheer per Markdown bestand zeer mogelijk is mbv Git
- Goed, omdat versiebeheer over een collectie Markdown bestanden ook zeer mogelijk is door de
  toepassing van Git
- Goed, omdat de brondocumenten in Markdown echt ontkoppeld zijn van de tool voor publicatie in HTML
- Slecht, omdat de brondocumenten op enige afstand kunnen staan door de ontkoppeling met de
  publicatie tool
- Slecht, omdat het versiebeheer geen onderdeel is van Markdown en extra toegevoegd moet worden. Git
  is daarvoor zeer geschikt ... maar ook zo uitgebreid, dat minder-technische mensen daar moeite mee
  hebben. Dit verhoogt de barrière voor minder-technische mensen om Markdown (plus Git) te gebruiken
- Vergelijkbaar met Wikitekst, omdat het platte tekst is waarin met codes opmaak wordt geproduceerd

### GitHub

> // TODO ...

Pros:
- Wel primaire oplossing voor de ontwikkeling van open source producten

Cons:
- Zelf geen open source oplossing

### GitLab

> // TODO ...

Pros:
- In de basis zelf een open source oplossing
- GitLab.com is secundaire oplossing voor de ontwikkeling van open source producten
- GitLab is ook zelf te hosten, maar dan is externe user management _ons_ probleem

Cons:
- GitLab is ook zelf te hosten, maar dan is externe user management _ons_ probleem

### NORA Wiki

Een wiki is een manier om online content te publiceren én tegelijk dat te beheren met meerdere
mensen. Het biedt de mogelijkheid om door content te bladeren, te zoeken en links tussen pagina's te
maken. Een 'What You See Is What You Get' (WYSIWYG) editor ondersteunt niet-technische gebruikers.
Dit is waar [Wikipedia](https://www.wikipedia.org/) mee ontstaan en groot geworden is.

De Nederlandse Overheids Referentie Architectuur, NORA, maakt gebruik van dezelfde technologie als
Wikipedia: [Mediawiki](https://www.mediawiki.org/wiki/MediaWiki). Het doel is vergelijkbaar als met
Wikipedia, namelijk dat het beheer van content eenvoudig is en door iedereen gedaan kan worden.

- Goed, omdat gepubliceerde content en het wijzigen van content dicht bij elkaar staan en te vinden
  zijn
- Goed, omdat niet-technische gebruikers gemakkelijk kunnen bijdragen (na aanmaken van een account,
  dat zelfstandig gedaan kan worden)
- Slecht, omdat versiebeheer per pagina niet zo duidelijk is
- Slecht, omdat versiebeheer van meerdere en samenhangende pagina's eigenlijk niet goed ondersteund
  worden (er zijn wel wat mogelijkheden ... maar allemaal suboptimaal)
- Vergelijkbaar met Markdown, omdat het onderliggende formaat waarmee een wiki werkt, ondanks de
  WYSIWYG editor, gebaseerd is op 'wikitekst'. Dat is de facto standaard van Mediawiki en lijkt op
  Markdown. Wikitekst is minder gestandaardiseerd dan Markdown en alleen (in deze vorm) ondersteund
  door Mediawiki.

### Static website

Voor het beschikbaar maken van de static website (zie ook [Markdown met Git](#markdown-met-git)) is
een (host)naam, een URL nodig. Wat zou een goede en toekomstbestendige naam zijn? De volgende
alternatieven zijn overwogen.

- `datastelsel.nl`
  - het doel waar we naartoe werken, het Nederlandse datastelsel (💪)
  - doel klopt, maar daar zijn we nog niet
  - branding is het nog te vroeg voor ...
- `ontwikkeling.datastelsel.nl`
  - de ontwikkeling van het Nederlandse Datastelsel
  - Realisatie IBDS zou dan te vinden moeten zijn onder `datastrategie.datastelsel.nl`?
  - past bij het einddoel
  - `ontwikkeling` is geen herkenbare brand op dit moment
- `federatief.datastelsel.nl` **<- keuze**
  - de ontwikkeling van het Nederlandse federatief datastelsel
  - Realisatie IBDS zou dan te vinden kunnen zijn onder `datastrategie.datastelsel.nl`?
  - huidige branding blijft gelijk !!
- `ontwikkelingfds.nl`
  - de ontwikkeling van FDS, het federatief datastelsel
  - helemaal losstaande URL ... kan zowel positief als negatief gezien worden
- `overheidsdatastelsel.nl`
  - het doel waar we (dan) naartoe werken, is het Nederlandse _Overheids_ datastelsel ... maar dat
  is niet zo. Ja, het is / begint bij overheid, maar het einddoel is weldegelijk overstijgend cq te
  kunnen verbinden met andere stelsels
- `ontwikkeling.overheidsdatastelsel.nl`
  - combinatie van bovenstaande ... met combinatie van die afwegingen
- `datastelsel.overheid.nl`
  - het doel waar we naartoe werken, het Nederlandse _Overheids_ datastelsel
  - onder `overheid.nl` is goed
  - maar het einddoel is niet alleen overheid ...
- `ontwikkeling.datastelsel.overheid.nl`
  - bovenstaande, maar dan de ontwikkeling daarvan
  datastelsel
- `federatiefdatastelsel.realisatieibds.nl`
  - FDS als onderdeel van Realisatie IBDS (onder eigen URL)
  - Realisatie IBDS zou dan te vinden moeten zijn onder `realisatieibds.nl`
  - Tja ... wel heel andere en losstaande richting ...

## Links <!-- optional -->

- doc [Strategie van samenwerken](/docs/strategie-van-samenwerken/)
- doc [Raamwerk (voor ontwikkeling in samenwerking)](/kennisbank/raamwerk/)
- doc [Werkomgeving](/docs/werkomgeving/)
